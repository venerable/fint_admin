<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('login');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

// Borrowers' Route
Route::get('/borrowers/fetch', 'BorrowerController@index');
Route::get('/borrowers/filter', 'BorrowerController@filter');
Route::post('/borrowers/verify', 'BorrowerController@verify');
Route::post('/borrowers/unverify', 'BorrowerController@unverify');
Route::post('/borrowers/resetbvnverifystatus', 'BorrowerController@resetBvnVerifyStatus');
Route::post('/borrowers/sendmail', 'BorrowerController@sendMail');
Route::post('/borrowers/sendsms', 'BorrowerController@sendSMS');
Route::post('/borrowers/editcomment', 'BorrowerController@editComment');
Route::post('/borrowers/deletecomment', 'BorrowerController@deleteComment');
Route::post('/borrowers/editDetails', 'BorrowerController@editDetails');

// Investors' Route
Route::get('/lenders/fetch', 'InvestorController@index');
Route::get('/lenders/filter', 'InvestorController@filter');

// Loans' Route
Route::get('/loans/fetch', 'LoanRequestController@index');
Route::get('/loans/filter', 'LoanRequestController@filter');
Route::post('/loans/fetchproposal', 'LoanRequestController@fetchproposal');
Route::post('/loans/clear', 'LoanRequestController@clear');
Route::post('/loans/cancel', 'LoanRequestController@cancel');
Route::post('/loans/markasdefault', 'LoanRequestController@markAsDefault');
Route::post('/loans/verify', 'LoanRequestController@verify');
Route::post('/loans/unverify', 'LoanRequestController@unverify');
Route::post('/loans/fundloan', 'LoanRequestController@fundLoan');
Route::get('/loans/repaid/filter', 'RepaidLoanRequestController@filter');
Route::post('/loans/repaid/fetchproposal', 'RepaidLoanRequestController@fetchproposal');
Route::get('/loans/expired/filter', 'ExpiredLoanRequestController@filter');
Route::post('/loans/expired/fetchproposal', 'ExpiredLoanRequestController@fetchproposal');
Route::get('/loans/canceled/filter', 'CanceledLoanRequestController@filter');
Route::post('/loans/canceled/fetchproposal', 'CanceledLoanRequestController@fetchproposal');
Route::get('/loans/defaulted/filter', 'DefaultedLoanRequestController@filter');
Route::post('/loans/defaulted/fetchproposal', 'DefaultedLoanRequestController@fetchproposal');
Route::post('/loans/moveoutofdefault', 'DefaultedLoanRequestController@moveOutOfDefault');
Route::post('/loans/confirmpayback', 'LoanRequestController@confirmPayback');
Route::post('/loans/fetchtestanswers', 'LoanRequestController@fetchTestAnswers');

//Schedule' Route
Route::get('/schedule/fetch', 'ScheduleController@index');
Route::get('/schedule/filter', 'ScheduleController@filter');

//Transactions' Route
Route::get('/transactions/fetch', 'TransactionsController@index');
Route::get('/transactions/filter', 'TransactionsController@filter');
