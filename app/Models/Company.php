<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:23 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Company
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $street_address
 * @property string $city
 * @property \Carbon\Carbon $date_created
 * @property int $interest_reduction
 *
 * @package App\Models
 */
class Company extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'interest_reduction' => 'int'
	];

	protected $dates = [
		'date_created'
	];

	protected $fillable = [
		'name',
		'description',
		'street_address',
		'city',
		'date_created',
		'interest_reduction'
	];
}
