<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:25 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DefaultStatus
 * 
 * @property int $id
 * @property string $status_meta
 * @property string $status_desc
 *
 * @package App\Models
 */
class DefaultStatus extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'status_meta',
		'status_desc'
	];
}
