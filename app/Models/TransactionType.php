<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TransactionType
 * 
 * @property int $id
 * @property string $type
 * @property string $description
 *
 * @package App\Models
 */
class TransactionType extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'type',
		'description'
	];
}
