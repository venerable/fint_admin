<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 12 Jun 2018 09:09:19 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BorrowerLoanDatum
 *
 * @property int $loan_data_id
 * @property int $borrower_id
 * @property int $locative
 * @property int $dependants
 * @property int $location
 * @property int $occupation
 * @property int $social_media
 * @property int $education
 * @property int $current_employer
 * @property int $prior_employer
 * @property int $maritial_status
 * @property int $length_of_account
 * @property int $cash_inflow
 * @property int $cash_outflow
 * @property int $occ_sal
 * @property int $industry
 * @property string $last_date_attempted
 * @property string $attempt_status
 *
 * @package App\Models
 */
class BorrowerLoanDatum extends Eloquent
{
    protected $primaryKey = 'loan_data_id';
    public $timestamps = FALSE;

    protected $casts = [
        'borrower_id' => 'int',
        'locative' => 'int',
        'dependants' => 'int',
        'location' => 'int',
        'occupation' => 'int',
        'social_media' => 'int',
        'education' => 'int',
        'current_employer' => 'int',
        'prior_employer' => 'int',
        'maritial_status' => 'int',
        'length_of_account' => 'int',
        'cash_inflow' => 'int',
        'cash_outflow' => 'int',
        'occ_sal' => 'int',
        'industry' => 'int'
    ];

    protected $fillable = [
        'borrower_id',
        'locative',
        'dependants',
        'location',
        'occupation',
        'social_media',
        'education',
        'current_employer',
        'prior_employer',
        'maritial_status',
        'length_of_account',
        'cash_inflow',
        'cash_outflow',
        'occ_sal',
        'industry',
        'last_date_attempted',
        'attempt_status'
    ];

    public function getLocativeAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'Own House (fully paid for)';
            case 1:
                return 'Own House (mortgaged)';
            case 2:
                return 'Rented House';
            case 3:
                return 'Owned Apartment';
            case 4:
                return 'Rented Apartment';
            case 5:
                return 'Shared Living';
            case 6:
                return 'Unpaid Living';
        }
    }

    public function getDependantsAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'No Dependants';
            case 1:
                return '1 Dependants';
            case 2:
                return '2 Dependants';
            case 3:
                return '3 Dependants';
            case 4:
                return '4 Dependants';
            case 5:
                return '5 Dependants';
            case 6:
                return '6 Dependants';
        }
    }

    public function getOccupationAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'Salaried Employee';
            case 1:
                return 'Business Owner';
            case 2:
                return 'Student';
            case 3:
                return 'Unemployed';
            case 4:
                return 'Contract/Informal Worker';
        }
    }

    public function getEducationAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'Doctorate';
            case 1:
                return 'Masters/M-PHD';
            case 2:
                return 'Undergraduate';
            case 3:
                return 'Graduate';
            case 4:
                return 'High-School Leaver';
            case 5:
                return 'Trademan';
            case 6:
                return 'Other';
        }
    }

    public function getCurrentEmployerAttribute($value)
    {
        switch ($value) {
            case 0:
                return '10 Years +';
            case 1:
                return '7 - 9 Years';
            case 2:
                return '4 - 6 Years';
            case 3:
                return '1 - 3 Years';
            case 4:
                return 'Less Than 1 Year';
            case 5:
                return 'Retired';
            case 6:
                return 'Unemployed';
            case 7:
                return 'Contract/Informal Worker';
        }
    }

    public function getPriorEmployerAttribute($value)
    {
        switch ($value) {
            case 0:
                return '5 Years +';
            case 1:
                return '2 - 5 Years';
            case 2:
                return '1 - 2 Years';
            case 3:
                return 'Less Than 1 Year';
            case 4:
                return 'Retired';
            case 5:
                return 'Self-Employed';
            case 6:
                return 'No Previous Employer';
        }
    }

    public function getMaritialStatusAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'Single';
            case 1:
                return 'Married';
            case 2:
                return 'Divorced (Paying Alimony)';
            case 3:
                return 'Divorced';
            case 4:
                return 'Widow/Widower';
        }
    }

    public function getLengthOfAccountAttribute($value)
    {
        switch ($value) {
            case 1:
                return '108 - 119 Months';
            case 2:
                return '96 - 107 Months';
            case 3:
                return '84 - 95 Months';
            case 4:
                return '65 - 83 Months';
            case 5:
                return '49 - 64 Months';
            case 6:
                return '37 - 48 Months';
            case 7:
                return '24 - 36 Months';
            case 8:
                return 'Less Than 23 Months';
        }
    }

    public function getIndustryAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'Advertising Agencies';
            case 1:
                return 'Agriculture';
            case 2:
                return 'Airlines';
            case 3:
                return 'Apparel Retailers';
            case 4:
                return 'Automobile/ Auto Part retailers';
            case 5:
                return 'Beauty';
            case 6:
                return 'Construction/ Real Estate';
            case 7:
                return 'Consumer Goods/ Manufacturing';
            case 8:
                return 'Courier/ Freight/ Delivery';
            case 9:
                return 'Education/ Training';
            case 10:
                return 'Electronics/ Appliances Retailers';
            case 11:
                return 'Employment Solutions';
            case 12:
                return 'Financial Services';
            case 13:
                return 'Food/ Drug Retailers and Wholesalers';
            case 14:
                return 'Government';
            case 15:
                return 'Healthcare';
            case 16:
                return 'Hospitality';
            case 17:
                return 'Industrial Goods';
            case 18:
                return 'Information and Communications Technology (includes telecoms)';
            case 19:
                return 'Media/ Entertainment';
            case 20:
                return 'Natural Resources';
            case 21:
                return 'Oil and Gas';
            case 22:
                return 'Printing/ Publishing';
            case 23:
                return 'Rail Transportation';
            case 24:
                return 'Repair/ maintenance';
            case 25:
                return 'Road Transportation';
            case 26:
                return 'Storage/ Warehousing';
            case 27:
                return 'Transport-related Services';
            case 28:
                return 'Travel and Tourism';
            case 29:
                return 'Utilities';
            case 30:
                return 'Waste Management';
            case 31:
                return 'Water Transportation';
        }
    }
}
