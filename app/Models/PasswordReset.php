<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:30 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PasswordReset
 * 
 * @property int $user_id
 * @property string $token
 * @property int $type
 * @property int $id
 *
 * @package App\Models
 */
class PasswordReset extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'type' => 'int',
		'id' => 'int'
	];

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'user_id',
		'token',
		'type'
	];
}
