<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SystemPref
 * 
 * @property int $id
 * @property string $meta
 * @property string $value
 *
 * @package App\Models
 */
class SystemPref extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'meta',
		'value'
	];
}
