<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:29 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Notification
 * 
 * @property int $NotID
 * @property int $Executor
 * @property int $ExecutorType
 * @property int $Receiver
 * @property int $ReceiverType
 * @property int $NotType
 * @property string $NotString
 * @property \Carbon\Carbon $Date
 * @property int $Seen
 * @property int $RelatedNote
 * @property int $RelatedLoan
 *
 * @package App\Models
 */
class Notification extends Eloquent
{
    protected $primaryKey = 'NotID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'NotID' => 'int',
		'Executor' => 'int',
		'ExecutorType' => 'int',
		'Receiver' => 'int',
		'ReceiverType' => 'int',
		'NotType' => 'int',
		'Seen' => 'int',
		'RelatedNote' => 'int',
		'RelatedLoan' => 'int'
	];

	protected $dates = [
		'Date'
	];

	protected $fillable = [
		'Executor',
        'Receiver',
		'ExecutorType',
		'ReceiverType',
		'NotType',
		'NotString',
		'Date',
		'Seen',
		'RelatedNote',
		'RelatedLoan'
	];
}
