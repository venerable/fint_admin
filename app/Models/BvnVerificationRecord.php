<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 06 Jun 2018 08:06:38 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BvnVerificationRecord
 * 
 * @property int $bvn_id
 * @property int $borrower
 * @property string $f_name
 * @property string $l_name
 * @property string $dob
 * @property string $bvn
 * @property string $phone
 * @property int $attempt
 * @property string $date_queried
 *
 * @package App\Models
 */
class BvnVerificationRecord extends Eloquent
{
	protected $primaryKey = 'bvn_id';
	public $timestamps = false;

	protected $casts = [
		'borrower' => 'int',
		'attempt' => 'int'
	];

	protected $fillable = [
		'borrower',
		'f_name',
		'l_name',
		'dob',
		'bvn',
		'phone',
		'attempt',
		'date_queried'
	];

    /**
     * Get the borrower that owns this BVN record
     */
    public function bvnDetails()
    {
        return $this->hasOne('App\Models\Borrower', 'borrower');
    }
}
