<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:23 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CardType
 * 
 * @property int $id
 * @property string $slug
 * @property string $name
 *
 * @package App\Models
 */
class CardType extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'slug',
		'name'
	];
}
