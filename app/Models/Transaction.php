<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:34 +0000.
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Transaction
 *
 * @property int $id
 * @property int $transaction_type
 * @property int $from
 * @property int $to
 * @property \Carbon\Carbon $date_created
 * @property int $payback_id
 * @property string $description
 * @property float $amount
 * @property int $profile
 * @property int $user_id
 * @property string $confirmation
 * @property string $related_loan
 * @property int $refunded
 *
 * @package App\Models
 */
class Transaction extends Eloquent
{
    public $incrementing = TRUE;
    public $timestamps = FALSE;

    protected $casts = [
        'id' => 'int',
        'transaction_type' => 'int',
        'from' => 'int',
        'to' => 'int',
        'payback_id' => 'int',
        'amount' => 'float',
        'profile' => 'int',
        'user_id' => 'int',
        'refunded' => 'int'
    ];

    protected $dates = [
        'date_created'
    ];

    protected $fillable = [
        'transaction_type',
        'from',
        'to',
        'date_created',
        'payback_id',
        'description',
        'user_id',
        'amount',
        'profile',
        'confirmation',
        'related_loan',
        'refunded'
    ];


    /**
     * Get the loan associated with this transaction.
     */
    public function loan()
    {
        return $this->belongsTo('App\Models\LoanRequest', 'related_loan');
    }

    /**
     * Get the type of transaction.
     */
    public function transaction_type()
    {
        return $this->belongsTo('App\Models\TransactionType', 'transaction_type');
    }

    /**
     * Scoped Queries
     */

    public function scopeLoanID($query, $loan_id)
    {
        if (empty($loan_id)) {
            return $query;
        }

        return $query->where('transactions.related_loan', 'LIKE', $loan_id . '%');
    }

    public function scopeUserID($query, $user_id)
    {
        if (empty($user_id)) {
            return $query;
        }

        return $query->where('transactions.user_id', 'LIKE', $user_id . '%');
    }

    public function scopeProfileType($query, $profile_type)
    {
        if (empty($profile_type)) {
            return $query;
        }

        return $query->where('transactions.profile', '=', DB::raw($profile_type));
    }

    public function scopeTransactionType($query, $transaction_type)
    {
        if (empty($transaction_type)) {
            return $query;
        }

        return $query->where('transactions.transaction_type', '=', DB::raw($transaction_type));
    }

    public function scopeReference($query, $reference)
    {
        if (empty($reference)) {
            return $query;
        }

        return $query->where('transactions.confirmation', 'LIKE', $reference . '%');
    }

    public function scopeFromDate($query, $dateFrom)
    {
        if (empty($dateFrom)) {
            return $query;
        }

        $dateFrom = date("Y-m-d H:i:s", strtotime(substr($dateFrom,0,10)));

        return $query->where('transactions.date_created', '>=', DB::raw("DATE('$dateFrom')"));
    }

    public function scopeToDate($query, $dateTo)
    {
        if (empty($dateTo)) {
            return $query;
        }

        $timestamp = null;

        if (strlen($dateTo) == 24) {
            $timestamp = strtotime(substr($dateTo,0,10)) + 60 * 60 * 24;
        } else {
            $timestamp = strtotime(substr($dateTo,0,10));
        }

        $dateTo = date("Y-m-d H:i:s", $timestamp + 60 * 60 * 24);

        return $query->where('transactions.date_created', '<', DB::raw("DATE('$dateTo')"));
    }

}
