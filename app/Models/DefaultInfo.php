<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:24 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DefaultInfo
 * 
 * @property int $id
 * @property int $loan_id
 * @property int $status
 * @property string $notes
 *
 * @package App\Models
 */
class DefaultInfo extends Eloquent
{
	protected $table = 'default_info';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'loan_id' => 'int',
		'status' => 'int'
	];

	protected $fillable = [
        'loan_id',
		'status',
		'notes'
	];

    /**
     * Get the status of this Defaulted Info
     */
    public function status()
    {
        return $this->belongsTo('App\Models\DefaultStatus', 'status');
    }
}
