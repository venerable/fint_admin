<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:28 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LoanRequestToken
 * 
 * @property int $token_id
 * @property int $owner_id
 * @property \Carbon\Carbon $date_purchased
 * @property int $used
 * @property \Carbon\Carbon $date_used
 *
 * @package App\Models
 */
class LoanRequestToken extends Eloquent
{
    protected $primaryKey = 'token_id';

	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'token_id' => 'int',
		'owner_id' => 'int',
		'used' => 'int'
	];

	protected $dates = [
		'date_purchased',
		'date_used'
	];

	protected $fillable = [
		'date_purchased',
		'used',
		'date_used'
	];

    /**
     * Get the borrower that has the token.
     */
    public function borrower()
    {
        return $this->belongsTo('App\Models\Borrower', 'owner_id');
    }
}
