<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:20 +0000.
 */

namespace App\Models;

// use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Admin
 * 
 * @property int $id
 * @property string $name
 * @property int $type
 * @property string $username
 * @property string $password
 * @property \Carbon\Carbon $date_joined
 * @property string $transaction_id
 * @property string $remember_token
 *
 * @package App\Models
 */
class Admin extends Authenticatable
{
	use Notifiable;
	
	public $timestamps = false;

	protected $casts = [
		'type' => 'int'
	];

	protected $dates = [
		'date_joined'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'type',
		'username',
		'password',
		'date_joined',
		'transaction_id',
		'remember_token'
	];
}
