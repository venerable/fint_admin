<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:30 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PhoneOtp
 * 
 * @property int $id
 * @property string $otp
 * @property int $user_id
 * @property string $timestamp
 *
 * @package App\Models
 */
class PhoneOtp extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'otp',
		'user_id',
		'timestamp'
	];
}
