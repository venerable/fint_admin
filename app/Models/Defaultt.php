<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:25 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Default
 *
 * @property int $id
 * @property int $borrower
 * @property string $borrower_risk_score
 * @property string $loan_grade
 * @property int $purpose_tag
 * @property string $interest_rate
 * @property int $term
 * @property int $time_elapsed
 * @property int $amount
 * @property int $funded_amount
 * @property int $funding_time_elapsed
 * @property int $number_of_investors
 * @property string $date_created
 * @property string $write_up
 * @property \Carbon\Carbon $date_disbursed
 * @property int $disbursed
 * @property int $payback_due
 * @property int $payback_amount
 * @property int $active
 * @property string $title
 * @property int $status
 * @property \Carbon\Carbon $date_deleted
 * @property int $insured
 * @property int $disb_clear
 *
 * @package App\Models
 */
class Defaultt extends Eloquent
{
    protected $table = 'defaults';

    public $incrementing = FALSE;
    public $timestamps = FALSE;

    protected $casts = [
        'id' => 'int',
        'borrower' => 'int',
        'purpose_tag' => 'int',
        'term' => 'int',
        'time_elapsed' => 'int',
        'amount' => 'int',
        'funded_amount' => 'int',
        'funding_time_elapsed' => 'int',
        'number_of_investors' => 'int',
        'disbursed' => 'int',
        'payback_due' => 'int',
        'payback_amount' => 'int',
        'active' => 'int',
        'status' => 'int',
        'insured' => 'int',
        'disb_clear' => 'int'
    ];

    protected $dates = [
        'date_created',
		'date_disbursed',
//		'date_deleted'
    ];

    protected $fillable = [
        'id',
        'borrower',
        'borrower_risk_score',
        'loan_grade',
        'purpose_tag',
        'interest_rate',
        'term',
        'time_elapsed',
        'amount',
        'funded_amount',
        'funding_time_elapsed',
        'number_of_investors',
        'date_created',
        'write_up',
        'date_disbursed',
        'disbursed',
        'payback_due',
        'payback_amount',
        'active',
        'title',
        'status',
        'date_deleted',
        'insured',
        'disb_clear'
    ];

    /**
     * Get the borrower that created the feedback.
     */
    public function borrower()
    {
        return $this->belongsTo('App\Models\Borrower', 'borrower');
    }

    /**
     * Get the files uploaded for this loan.
     */
    public function files()
    {
        return $this->hasMany('App\Models\File', 'loan_id');
    }

    /**
     * Get the files uploaded for this loan.
     */
    public function purpose()
    {
        return $this->belongsTo('App\Models\Purpose', 'purpose_tag');
    }

    /**
     * Get the paybacks associated with this loan.
     */
    public function paybacks()
    {
        return $this->hasMany('App\Models\DefaultPayback', 'loan_id');
    }

    /**
     * Get the investments associated with this loan.
     */
    public function investments()
    {
        return $this->hasMany('App\Models\DefaultNote', 'loan_id');
    }

    /**
     * Scoped Queries
     */
    public function scopeId($query, $id)
    {
        if (empty($id)) {
            return $query;
        }

        return $query->where('defaults.id', 'LIKE', $id . '%');
    }

    public function scopeBorrowerID($query, $borrower_id)
    {
        if (empty($borrower_id)) {
            return $query;
        }

        return $query->where('defaults.borrower', 'LIKE', $borrower_id . '%');
    }

    public function scopeLoanTitle($query, $loan_title)
    {
        if (empty($loan_title)) {
            return $query;
        }

        return $query->where('defaults.title', 'LIKE', $loan_title . '%');
    }

    public function scopeLoanGrade($query, $loan_grade)
    {
        if (empty($loan_grade)) {
            return $query;
        }

        return $query->where('defaults.loan_grade', $loan_grade);
    }

    public function scopeInterestRate($query, $interest_rate)
    {
        if (empty($interest_rate)) {
            return $query;
        }

        return $query->where('defaults.interest_rate', '>=', DB::raw($interest_rate));
    }

    public function scopeFromAmount($query, $from_amount)
    {
        if (empty($from_amount)) {
            return $query;
        }

        return $query->where('defaults.amount', '>=', DB::raw($from_amount));
    }

    public function scopeToAmount($query, $to_amount)
    {
        if (empty($to_amount)) {
            return $query;
        }

        return $query->where('defaults.amount', '<=', DB::raw($to_amount));
    }

    public function scopeFromDate($query, $dateFrom)
    {
        if (empty($dateFrom)) {
            return $query;
        }

        $dateFrom = date("Y-m-d H:i:s", strtotime($dateFrom . ' UTC'));

        return $query->where('defaults.date_created', '>=', $dateFrom);
    }

    public function scopeToDate($query, $dateTo)
    {
        if (empty($dateTo)) {
            return $query;
        }

        $dateTo = date("Y-m-d H:i:s", strtotime($dateTo . ' UTC'));

        return $query->where('defaults.date_created', '<', $dateTo);
    }

    public function scopeFunded($query, $funded)
    {
        if (empty($funded)) {
            return $query;
        }

        if ($funded == 'not_fully_funded') {
            return $query->whereColumn('defaults.funded_amount', '<', 'defaults.amount')
                ->where('defaults.funded_amount', '>', DB::raw(0));
        } else if ($funded == 'fully_funded') {
            return $query->whereColumn('defaults.funded_amount', '=', 'defaults.amount');
        } else if ($funded == 'has_disbursed') {
            return $query->where('defaults.disbursed', DB::raw(1));
        } else if ($funded == 'fully_funded_not_disbursed') {
            return $query->where('defaults.disbursed', '=', DB::raw(0))
                ->whereColumn('defaults.funded_amount', '=', 'defaults.amount');
        }
    }

    public function scopeHasDisbursed($query, $disbursed)
    {
        if (empty($disbursed)) {
            return $query;
        }

        if ($disbursed == 'has_disbursed') {
            return $query->where('defaults.disbursed', DB::raw(1));
        } else {
            return $query->where('defaults.disbursed', '<>', DB::raw(1));
        }

    }

    public function scopeHasFiles($query, $has_files)
    {
        if (empty($has_files)) {
            return $query;
        }

        return $query->where('defaults.active', DB::raw(1));

    }

    public static function insertIgnore($data)
    {
        $self = new static();

        $now = \Carbon\Carbon::now();
        $data['date_deleted'] = $now;

        \DB::insert('INSERT IGNORE INTO ' . $self->getTable() . ' (' . implode(',', array_keys($data)) .
            ') values (?' . str_repeat(',?', count($data) - 1) . ')', array_values($data));
    }
}
