<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:21 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BorrowerAnswer
 * 
 * @property int $id
 * @property int $borrower_id
 * @property string $file
 * @property \Carbon\Carbon $date
 *
 * @package App\Models
 */
class BorrowerAnswer extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'borrower_id' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'borrower_id',
		'file',
		'date'
	];
}
