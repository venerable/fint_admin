<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:30 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Price
 * 
 * @property int $id
 * @property string $price_meta
 * @property float $amount
 * @property string $currency
 * @property int $perc_or_int
 * @property int $plus
 *
 * @package App\Models
 */
class Price extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'amount' => 'float',
		'perc_or_int' => 'int',
		'plus' => 'int'
	];

	protected $fillable = [
		'price_meta',
		'amount',
		'currency',
		'perc_or_int',
		'plus'
	];
}
