<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Bank
 * 
 * @property int $id
 * @property string $bank_name
 * @property string $flutterwave_bankcode
 * @property \Carbon\Carbon $date_added
 *
 * @package App\Models
 */
class Bank extends Eloquent
{

	public $timestamps = false;

	protected $dates = [
		'date_added'
	];

	protected $fillable = [
		'bank_name',
		'flutterwave_bankcode',
		'date_added'
	];

    /**
     * Get the loan created by the borrower.
     */
    public function accounts()
    {
        return $this->hasMany('App\Models\BankInfo', 'bank_id');
    }
}
