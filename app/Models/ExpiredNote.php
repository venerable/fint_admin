<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ExpiredNote
 * 
 * @property int $id
 * @property int $amount_in
 * @property int $investor_id
 * @property string $date_created
 * @property int $loan_id
 * @property int $total_return
 * @property \Carbon\Carbon $date_voided
 * @property int $status
 *
 * @package App\Models
 */
class ExpiredNote extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'amount_in' => 'int',
		'investor_id' => 'int',
		'loan_id' => 'int',
		'total_return' => 'int',
		'status' => 'int'
	];

	protected $dates = [
		'date_voided'
	];

	protected $fillable = [
		'amount_in',
		'investor_id',
		'date_created',
		'total_return',
		'date_voided',
		'status'
	];

    /**
     * Get the lender that owns the note.
     */
    public function lender()
    {
        return $this->belongsTo('App\Models\Investor', 'investor_id');
    }

    /**
     * Get the loan associated with this note.
     */
    public function loan()
    {
        return $this->belongsTo('App\Models\ExpiredLoanRequest', 'loan_id');
    }
}
