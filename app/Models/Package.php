<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:29 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Package
 * 
 * @property int $id
 * @property string $package_name
 * @property string $package_description
 * @property string $package_slug
 *
 * @package App\Models
 */
class Package extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'package_name',
		'package_description',
		'package_slug'
	];
}
