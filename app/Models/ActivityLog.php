<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ActivityLog
 * 
 * @property int $id
 * @property int $user_id
 * @property int $type
 * @property string $action
 * @property int $related_loan
 * @property \Carbon\Carbon $date
 *
 * @package App\Models
 */
class ActivityLog extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'type' => 'int',
		'related_loan' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'user_id',
		'type',
		'action',
		'related_loan',
		'date'
	];
}
