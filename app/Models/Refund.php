<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:31 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Refund
 * 
 * @property int $ref_id
 * @property int $user_id
 * @property int $borr_inv
 * @property string $reason
 * @property int $amount
 * @property string $reference
 * @property \Carbon\Carbon $date
 * @property string $name
 * @property string $bank_code
 * @property string $acc_num
 *
 * @package App\Models
 */
class Refund extends Eloquent
{
	protected $primaryKey = 'ref_id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'ref_id' => 'int',
		'user_id' => 'int',
		'borr_inv' => 'int',
		'amount' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'user_id',
		'borr_inv',
		'reason',
		'amount',
		'reference',
		'date',
		'name',
		'bank_code',
		'acc_num'
	];
}
