<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:24 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 * 
 * @property int $country_id
 * @property string $country_name
 *
 * @package App\Models
 */
class Country extends Eloquent
{
	protected $primaryKey = 'country_id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'country_id' => 'int'
	];

	protected $fillable = [
		'country_name'
	];
}
