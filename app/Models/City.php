<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:23 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 * 
 * @property int $city_id
 * @property string $city_name
 * @property int $state_id
 * @property int $points
 *
 * @package App\Models
 */
class City extends Eloquent
{
	protected $primaryKey = 'city_id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'city_id' => 'int',
		'state_id' => 'int',
		'points' => 'int'
	];

	protected $fillable = [
		'city_name',
		'state_id',
		'points'
	];

    /**
     * Get the state associated with this city.
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    /**
     * Get the borrowers' home associated with this city.
     */
    public function borrowersHome()
    {
        return $this->hasMany('App\Models\Borrowers', 'town_id');
    }

    /**
     * Get the borrowers' business associated with this city.
     */
    public function borrowersBusiness()
    {
        return $this->hasMany('App\Models\Borrowers', 'b_town_id');
    }
}
