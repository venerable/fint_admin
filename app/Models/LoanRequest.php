<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:28 +0000.
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LoanRequest
 *
 * @property int $id
 * @property int $borrower
 * @property string $borrower_risk_score
 * @property string $loan_grade
 * @property int $purpose_tag
 * @property string $interest_rate
 * @property int $term
 * @property int $time_elapsed
 * @property int $amount
 * @property int $funded_amount
 * @property int $funding_time_elapsed
 * @property int $number_of_investors
 * @property \Carbon\Carbon $date_created
 * @property string $write_up
 * @property \Carbon\Carbon $date_disbursed
 * @property int $disbursed
 * @property int $payback_due
 * @property int $payback_amount
 * @property int $active
 * @property string $title
 * @property int $status
 * @property \Carbon\Carbon $date_deleted
 * @property int $insured
 * @property int $disb_clear
 *
 * @package App\Models
 */
class LoanRequest extends Eloquent
{
    public $incrementing = FALSE;
    public $timestamps = FALSE;

    protected $casts = [
        'id' => 'int',
        'borrower' => 'int',
        'purpose_tag' => 'int',
        'term' => 'int',
        'time_elapsed' => 'int',
        'amount' => 'int',
        'funded_amount' => 'int',
        'funding_time_elapsed' => 'int',
        'number_of_investors' => 'int',
        'disbursed' => 'int',
        'payback_due' => 'int',
        'payback_amount' => 'int',
        'active' => 'int',
        'status' => 'int',
        'insured' => 'int',
        'disb_clear' => 'int'
    ];

    protected $dates = [
        'date_created',
//		'date_disbursed',
//        'date_deleted'
    ];

    protected $fillable = [
        'id',
        'borrower',
        'borrower_risk_score',
        'loan_grade',
        'purpose_tag',
        'interest_rate',
        'term',
        'time_elapsed',
        'amount',
        'funded_amount',
        'funding_time_elapsed',
        'number_of_investors',
        'date_created',
        'write_up',
        'date_disbursed',
        'disbursed',
        'payback_due',
        'payback_amount',
        'active',
        'title',
        'status',
        'date_deleted',
        'insured',
        'disb_clear'
    ];

    /**
     * Check for a particular join
     */
    public static function isJoined($query, $table)
    {
        return collect($query->getQuery()->joins)->pluck('table')->contains($table);
    }

    /**
     * Get the borrower that created the feedback.
     */
    public function borrower()
    {
        return $this->belongsTo('App\Models\Borrower', 'borrower');
    }

    /**
     * Get the files uploaded for this loan.
     */
    public function files()
    {
        return $this->hasMany('App\Models\File', 'loan_id');
    }

    /**
     * Get the files uploaded for this loan.
     */
    public function purpose()
    {
        return $this->belongsTo('App\Models\Purpose', 'purpose_tag');
    }

    /**
     * Get the paybacks associated with this loan.
     */
    public function paybacks()
    {
        return $this->hasMany('App\Models\Payback', 'loan_id');
    }


    /**
     * Get the investments associated with this loan.
     */
    public function investments()
    {
        return $this->hasMany('App\Models\Note', 'loan_id');
    }
    /**
     * Scoped Queries
     */
    public function scopeId($query, $id)
    {
        if (empty($id)) {
            return $query;
        }

        return $query->where('loan_requests.id', 'LIKE', $id . '%');
    }

    public function scopeBorrowerID($query, $borrower_id)
    {
        if (empty($borrower_id)) {
            return $query;
        }

        return $query->where('loan_requests.borrower', 'LIKE', $borrower_id . '%');
    }

    public function scopeInvestorID($query, $investor_id)
    {
        if (empty($investor_id)) {
            return $query;
        }

        return $query->whereExists(function ($query) use ($investor_id) {
            $query->selectRaw(1)
                ->from('notes')
                ->join('investors', 'investors.id', '=', DB::raw('notes.investor_id'))
                ->where('notes.loan_id', '=', DB::raw('loan_requests.id'))
                ->where('notes.investor_id', DB::raw($investor_id));
        });
    }

    public function scopeLoanTitle($query, $loan_title)
    {
        if (empty($loan_title)) {
            return $query;
        }

        return $query->where('loan_requests.title', 'LIKE', $loan_title . '%');
    }

    public function scopeLoanGrade($query, $loan_grade)
    {
        if (empty($loan_grade)) {
            return $query;
        }

        return $query->where('loan_requests.loan_grade', $loan_grade);
    }

    public function scopeInterestRate($query, $interest_rate)
    {
        if (empty($interest_rate)) {
            return $query;
        }

        return $query->where('loan_requests.interest_rate', '>=', DB::raw($interest_rate));
    }

    public function scopeLoanTerm($query, $loan_term)
    {
        if (empty($loan_term)) {
            return $query;
        }

        return $query->where('loan_requests.term', '=', DB::raw($loan_term));
    }

    public function scopeFromAmount($query, $from_amount)
    {
        if (empty($from_amount)) {
            return $query;
        }

        return $query->where('loan_requests.amount', '>=', DB::raw($from_amount));
    }

    public function scopeToAmount($query, $to_amount)
    {
        if (empty($to_amount)) {
            return $query;
        }

        return $query->where('loan_requests.amount', '<=', DB::raw($to_amount));
    }

    public function scopeFromDate($query, $dateFrom)
    {
        if (empty($dateFrom)) {
            return $query;
        }

        $dateFrom = date("Y-m-d H:i:s", strtotime(substr($dateFrom, 0, 10)));

        return $query->where('loan_requests.date_created', '>=', DB::raw("DATE('$dateFrom')"));
    }

    public function scopeToDate($query, $dateTo)
    {
        if (empty($dateTo)) {
            return $query;
        }

        $timestamp = NULL;

        if (strlen($dateTo) == 24) {
            $timestamp = strtotime(substr($dateTo, 0, 10)) + 60 * 60 * 24;
        } else {
            $timestamp = strtotime(substr($dateTo, 0, 10));
        }

        $dateTo = date("Y-m-d H:i:s", $timestamp + 60 * 60 * 24);

        return $query->where('loan_requests.date_created', '<', DB::raw("DATE('$dateTo')"));
    }

    public function scopeFunded($query, $funded)
    {
        if (empty($funded)) {
            return $query;
        }

        if ($funded == 'not_fully_funded') {
            return $query->whereColumn('loan_requests.funded_amount', '<', 'loan_requests.amount')
                ->where('loan_requests.active', '=', DB::raw(1))
                ->where('loan_requests.disb_clear', '=', DB::raw(1));
        } else if ($funded == 'fully_funded') {
            return $query->whereColumn('loan_requests.funded_amount', '=', 'loan_requests.amount');
        } else if ($funded == 'has_disbursed') {
            return $query->where('loan_requests.disbursed', DB::raw(1));
        } else if ($funded == 'fully_funded_not_disbursed') {
            return $query->where('loan_requests.disbursed', '=', DB::raw(0))
                ->whereColumn('loan_requests.funded_amount', '=', 'loan_requests.amount');
        }
    }

    public function scopeHasDisbursed($query, $disbursed)
    {
        if (empty($disbursed)) {
            return $query;
        }

        if ($disbursed == 'has_disbursed') {
            return $query->where('loan_requests.disbursed', DB::raw(1));
        } else {
            return $query->where('loan_requests.disbursed', '<>', DB::raw(1));
        }

    }

    public function scopeHasFiles($query, $has_files)
    {
        if (empty($has_files)) {
            return $query;
        }

        return $query->where('loan_requests.active', DB::raw(1));

    }

    public function scopeIsBvnVerified($query, $bvn_verified)
    {
        if ($bvn_verified === 'false') {
            return $query;
        }

        return $query->whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('borrowers')
                ->whereRaw('loan_requests.borrower = borrowers.id')
                ->where('borrowers.bvn_verified', '=', 1);
        });
    }

    public function scopeIsPhoneVerified($query, $phone_verified)
    {
        if ($phone_verified === 'false') {
            return $query;
        }

        return $query->whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('borrowers')
                ->whereRaw('loan_requests.borrower = borrowers.id')
                ->where('borrowers.phone_verified', '=', 1);
        });
    }

    public function scopeIsHomeVerified($query, $home_verified)
    {
        if ($home_verified === 'false') {
            return $query;
        }

        return $query->whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('borrowers')
                ->whereRaw('loan_requests.borrower = borrowers.id')
                ->where('borrowers.address_verified', '=', 1);
        });
    }

    public function scopeIsWorkVerified($query, $work_verified)
    {
        if ($work_verified === 'false') {
            return $query;
        }

        return $query->whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('borrowers')
                ->whereRaw('loan_requests.borrower = borrowers.id')
                ->where('borrowers.work_address_verified', '=', 1);
        });
    }

    public function scopeIsBankStatementVerified($query, $bank_stat_verified)
    {
        if ($bank_stat_verified === 'false') {
            return $query;
        }

        return $query->where('loan_requests.bank_stat_verified', '=', 1);
    }

}
