<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:21 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Behaviour
 * 
 * @property int $id
 * @property int $borrower_id
 * @property int $related_loan
 * @property \Carbon\Carbon $date_added
 * @property string $behaviour_meta
 * @property int $amount
 *
 * @package App\Models
 */
class Behaviour extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'borrower_id' => 'int',
		'related_loan' => 'int',
		'amount' => 'int'
	];

	protected $dates = [
		'date_added'
	];

	protected $fillable = [
		'related_loan',
		'date_added',
		'behaviour_meta',
		'amount'
	];
}
