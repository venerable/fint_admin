<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 08 Jun 2018 10:32:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminComment
 * 
 * @property int $id
 * @property int $admin_id
 * @property string $comment
 * @property string $entity
 * @property \Carbon\Carbon $date_created
 *
 * @package App\Models
 */
class AdminComment extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'admin_id' => 'int'
	];

	protected $dates = [
		'date_created'
	];

	protected $fillable = [
		'admin_id',
		'comment',
        'entity_id',
		'entity',
		'date_created'
	];

    /**
     * Get the admin that made the coment.
     */
    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'admin_id');
    }

}
