<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:24 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DefaultPayback
 * 
 * @property int $id
 * @property int $loan_id
 * @property int $installment
 * @property int $borrower_id
 * @property int $amount
 * @property string $set_date
 * @property int $attempts
 * @property int $success
 *
 * @package App\Models
 */
class DefaultPayback extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'loan_id' => 'int',
		'installment' => 'int',
		'borrower_id' => 'int',
		'amount' => 'int',
		'attempts' => 'int',
		'success' => 'int'
	];

	protected $fillable = [
        'loan_id',
		'installment',
		'borrower_id',
		'amount',
		'set_date',
		'attempts',
		'success'
	];

    /**
     * Get the loan associated with this payback.
     */
    public function loan()
    {
        return $this->belongsTo('App\Models\Defaultt', 'loan_id');
    }
}
