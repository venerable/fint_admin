<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ToBePaid
 * 
 * @property int $id
 * @property string $for_meta
 * @property int $amount
 * @property \Carbon\Carbon $date_added
 * @property int $to_who
 * @property int $attempts
 * @property int $success
 * @property \Carbon\Carbon $date_of_last_attempt
 * @property int $related_loan
 * @property int $related_note
 * @property int $profile
 *
 * @package App\Models
 */
class ToBePaid extends Eloquent
{
	protected $table = 'to_be_paid';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'amount' => 'int',
		'to_who' => 'int',
		'attempts' => 'int',
		'success' => 'int',
		'related_loan' => 'int',
		'related_note' => 'int',
		'profile' => 'int'
	];

	protected $dates = [
		'date_added',
//		'date_of_last_attempt'
	];

	protected $fillable = [
		'for_meta',
		'amount',
		'date_added',
		'to_who',
		'attempts',
		'success',
		'date_of_last_attempt',
		'related_loan',
		'related_note',
		'profile'
	];

    public static function insertIgnore($data)
    {
        $self = new static();

        $now = \Carbon\Carbon::now();
        $data['date_added'] = $now;
        $data['date_of_last_attempt'] = $now;

        \DB::insert('INSERT IGNORE INTO ' . $self->getTable() . ' (' . implode(',', array_keys($data)) .
            ') values (?' . str_repeat(',?', count($data) - 1) . ')', array_values($data));
    }
}
