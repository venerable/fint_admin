<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:30 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Purpose
 * 
 * @property int $id
 * @property string $purpose_name
 * @property string $purpose_description
 * @property int $financial_effect
 *
 * @package App\Models
 */
class Purpose extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'financial_effect' => 'int'
	];

	protected $fillable = [
		'purpose_name',
		'purpose_description',
		'financial_effect'
	];
}
