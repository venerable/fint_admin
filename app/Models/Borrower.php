<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Jul 2018 08:28:56 +0000.
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Borrower
 *
 * @property int $id
 * @property string $f_name
 * @property string $l_name
 * @property string $m_name
 * @property string $street_address
 * @property string $town
 * @property string $lga
 * @property string $state
 * @property string $country
 * @property string $age
 * @property string $business_address
 * @property string $business_town
 * @property string $business_lga
 * @property string $business_state
 * @property string $business_country
 * @property string $phone
 * @property string $business_phone
 * @property string $business_name
 * @property string $email
 * @property string $tin
 * @property int $phone_verified
 * @property int $address_verified
 * @property int $bvn_verified
 * @property int $work_address_verified
 * @property string $username
 * @property string $password
 * @property string $bio
 * @property string $bvn
 * @property int $dob_day
 * @property int $dob_month
 * @property int $dob_year
 * @property string $date_of_birth
 * @property int $confirmed
 * @property int $locked_to_company
 * @property int $gender
 * @property string $transaction_pin
 * @property \Carbon\Carbon $date_joined
 * @property int $company
 * @property string $employee_num
 * @property int $town_id
 * @property int $b_town_id
 * @property int $state_id
 * @property string $reference_name
 * @property string $reference_phone
 * @property string $reference_email
 * @property string $reference_social_handle
 * @property string $employment_status
 * @property int $salary_day
 * @property string $hr_phone
 * @property string $area
 * @property string $landmark
 * @property string $business_area
 * @property string $business_landmark
 * @property int $aff_id
 * @property int $referral_id
 *
 * @package App\Models
 */
class Borrower extends Eloquent
{
    public $timestamps = FALSE;

    protected $casts = [
        'phone_verified' => 'int',
        'address_verified' => 'int',
        'bvn_verified' => 'int',
        'work_address_verified' => 'int',
        'dob_day' => 'int',
        'dob_month' => 'int',
        'dob_year' => 'int',
        'confirmed' => 'int',
        'locked_to_company' => 'int',
        'gender' => 'int',
        'company' => 'int',
        'town_id' => 'int',
        'b_town_id' => 'int',
        'state_id' => 'int',
        'salary_day' => 'int',
        'aff_id' => 'int',
        'referral_id' => 'int'
    ];

    protected $dates = [
        'date_joined'
    ];

    protected $hidden = [
        'password'
    ];

    protected $fillable = [
        'f_name',
        'l_name',
        'm_name',
        'street_address',
        'town',
        'lga',
        'state',
        'country',
        'age',
        'business_address',
        'business_town',
        'business_lga',
        'business_state',
        'business_country',
        'phone',
        'business_phone',
        'business_name',
        'email',
        'tin',
        'phone_verified',
        'address_verified',
        'bvn_verified',
        'work_address_verified',
        'username',
        'password',
        'bio',
        'bvn',
        'dob_day',
        'dob_month',
        'dob_year',
        'date_of_birth',
        'confirmed',
        'locked_to_company',
        'gender',
        'transaction_pin',
        'date_joined',
        'company',
        'employee_num',
        'town_id',
        'b_town_id',
        'state_id',
        'reference_name',
        'reference_phone',
        'reference_email',
        'reference_social_handle',
        'employment_status',
        'salary_day',
        'hr_phone',
        'area',
        'landmark',
        'business_area',
        'business_landmark',
        'aff_id',
        'referral_id'
    ];

    /**
     * Get the loan created by the borrower.
     */
    public function requestedLoan()
    {
        return $this->hasOne('App\Models\LoanRequest', 'borrower');
    }

    /**
     * Get the loan data created by the borrower if any.
     */
    public function loanData()
    {
        return $this->hasMany('App\Models\BorrowerLoanDatum', 'borrower_id');
    }

    /**
     * Get the loan data created by the borrower if any.
     */
    public function tokens()
    {
        return $this->hasMany('App\Models\LoanRequestToken', 'owner_id');
    }

    /**
     * Get the home city of the borrower.
     */
    public function homeCity()
    {
        return $this->belongsTo('App\Models\City', 'town_id');
    }

    /**
     * Get the business city of the borrower.
     */
    public function businessCity()
    {
        return $this->belongsTo('App\Models\City', 'b_town_id');
    }

    /**
     * Get the BVN record of the borrower.
     */
    public function bvnDetails()
    {
        return $this->hasOne('App\Models\BvnVerificationRecord', 'borrower');
    }

    /**
     * Get the comment on this borrower.
     */
    public function commentInfo()
    {
        return $this->hasOne('App\Models\AdminComment', 'entity_id')
            ->where('entity', '=', 'BORROWER');
    }

    /**
     * Get the files uploaded by the borrower through the loan.
     */
    public function files()
    {
        return $this->hasManyThrough('App\Models\File', 'App\Models\LoanRequest',
            'borrower', 'loan_id');
    }

    /**
     * Get the account associated with this borrower.
     */
    public function accounts()
    {
        return $this->hasMany('App\Models\BankInfo', 'owner_id')
            ->where('card_account', '1')
            ->where('borrower_investor', '0');
    }

    /****************************/

    /**
     * Mutators
     */

    public function getEmploymentStatusAttribute($value)
    {
        if ($value == 'EMPLOYED')
            return 'Employed';
        else if ($value == 'EMPLOYED_BUSINESS')
            return 'Employed with Side Business';
        else if ($value == 'BUSINESS')
            return 'Business';
        else
            return $value;
    }

    /****************************/

    /**
     * Scoped Queries
     */
    public function scopeId($query, $id)
    {
        if (empty($id)) {
            return $query;
        }

        return $query->where('borrowers.id', 'LIKE', $id . '%');
    }

    public function scopeFirstName($query, $f_name)
    {
        if (empty($f_name)) {
            return $query;
        }

        return $query->where('borrowers.f_name', 'LIKE', $f_name . '%');
    }

    public function scopeLastName($query, $l_name)
    {
        if (empty($l_name)) {
            return $query;
        }

        return $query->where('borrowers.l_name', 'LIKE', $l_name . '%');
    }

    public function scopeUsername($query, $username)
    {
        if (empty($username)) {
            return $query;
        }

        return $query->where('borrowers.username', 'LIKE', $username . '%');
    }

    public function scopePhone($query, $phone)
    {
        if (empty($phone)) {
            return $query;
        }

        return $query->where('borrowers.phone', 'LIKE', $phone . '%');
    }

    public function scopeEmail($query, $email)
    {
        if (empty($email)) {
            return $query;
        }

        return $query->where('borrowers.email', 'LIKE', $email . '%');
    }

    public function scopeDateFrom($query, $dateFrom)
    {
        if (empty($dateFrom)) {
            return $query;
        }

        $dateFrom = date("Y-m-d H:i:s", strtotime(substr($dateFrom, 0, 10)));

        return $query->where('borrowers.date_joined', '>=', DB::raw("DATE('$dateFrom')"));
    }

    public function scopeDateTo($query, $dateTo)
    {
        if (empty($dateTo)) {
            return $query;
        }

        $timestamp = NULL;

        if (strlen($dateTo) == 24) {
            $timestamp = strtotime(substr($dateTo, 0, 10)) + 60 * 60 * 24;
        } else {
            $timestamp = strtotime(substr($dateTo, 0, 10));
        }

        $dateTo = date("Y-m-d H:i:s", $timestamp + 60 * 60 * 24);

        return $query->where('borrowers.date_joined', '<', DB::raw("DATE('$dateTo')"));
    }

    public function scopeHasTakenTest($query, $rat)
    {
        if (empty($rat)) {
            return $query;
        }

        if ($rat == 'no_test') {
            return $query->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('borrower_loan_data')
                    ->where('borrower_loan_data.borrower_id', '=', DB::raw('borrowers.id'));
            });
        } else if ($rat == 'taken_test') {
            return $query->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('borrower_loan_data')
                    ->where('borrower_loan_data.borrower_id', '=', DB::raw('borrowers.id'));
            });
        } else if ($rat == 'passed_test_old') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('loan_request_tokens')
                    ->leftJoin('loan_requests', 'loan_request_tokens.owner_id', '=', DB::raw('loan_requests.borrower'))
                    ->where('loan_request_tokens.owner_id', '=', DB::raw('borrowers.id'))
                    ->whereNotNull('loan_requests.id');
            });
        } else if ($rat == 'failed_test_old') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('loan_request_tokens')
                    ->leftJoin('loan_requests', 'loan_request_tokens.owner_id', '=', DB::raw('loan_requests.borrower'))
                    ->where('loan_request_tokens.owner_id', '=', DB::raw('borrowers.id'))
                    ->whereNull('loan_requests.id')
                    ->where('loan_request_tokens.used', '=', DB::raw(1));
            });
        } else if ($rat == 'passed_test_new') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('borrower_loan_data')
                    ->where('borrower_loan_data.attempt_status', '=', 'passed')
                    ->where('borrower_loan_data.borrower_id', '=', DB::raw('borrowers.id'));
            });
        } else if ($rat == 'failed_test_new') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('borrower_loan_data')
                    ->where('borrower_loan_data.attempt_status', '=', 'failed')
                    ->where('borrower_loan_data.borrower_id', '=', DB::raw('borrowers.id'));
            });
        } else if ($rat == 'passed_test_created_loan_new') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('borrower_loan_data')
                    ->leftJoin('loan_requests', 'borrower_loan_data.borrower_id', '=', DB::raw('loan_requests.borrower'))
                    ->where('borrower_loan_data.borrower_id', '=', DB::raw('borrowers.id'))
                    ->whereNotNull('loan_requests.id');
            });
        } else if ($rat == 'passed_test_no_loan_new') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('borrower_loan_data')
                    ->leftJoin('loan_requests', 'borrower_loan_data.borrower_id', '=', DB::raw('loan_requests.borrower'))
                    ->where('borrower_loan_data.borrower_id', '=', DB::raw('borrowers.id'))
                    ->whereNull('loan_requests.id');
            });
        }

    }

    public function scopeHasEmployment($query, $employment_status)
    {
        if (empty($employment_status)) {
            return $query;
        }

        return $query->where('borrowers.employment_status', strtoupper($employment_status));
    }

    public function scopeHasActiveLoan($query, $active)
    {
        if (empty($active)) {
            return $query;
        }

        if ($active == 'has_loan') {
            return $query->join('loan_requests', 'loan_requests.borrower', '=', 'borrowers.id')
                ->select('borrowers.*');
        } else {
            return $query->whereNotExists(function ($query) {
                $query->selectRaw(1)
                    ->from('borrowers AS b2')
                    ->join('loan_requests', 'loan_requests.borrower', '=', DB::raw('b2.id'))
                    ->where('borrowers.id', '=', DB::raw('b2.id'));
            });
        }

    }

    public function scopeHasFiles($query, $has_files)
    {
        if (empty($has_files)) {
            return $query;
        }

        if ($has_files == 'has_files') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('loan_requests')
                    ->where('loan_requests.borrower', DB::raw('borrowers.id'))
                    ->where('loan_requests.active', 1);
            });
        } else {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('loan_requests')
                    ->where('loan_requests.borrower', DB::raw('borrowers.id'))
                    ->where('loan_requests.active', 0);
            });
        }

    }

    public function scopeIsBvnVerified($query, $bvn_verified)
    {
        if ($bvn_verified === 'false') {
            return $query;
        }

        return $query->where('borrowers.bvn_verified', '=', \DB::raw(1));
    }

    public function scopeIsPhoneVerified($query, $phone_verified)
    {
        if ($phone_verified === 'false') {
            return $query;
        }

        return $query->where('borrowers.phone_verified', '=', \DB::raw(1));
    }

    public function scopeIsHomeVerified($query, $home_verified)
    {
        if ($home_verified === 'false') {
            return $query;
        }

        return $query->where('borrowers.address_verified', '=', \DB::raw(1));
    }

    public function scopeIsWorkVerified($query, $work_verified)
    {
        if ($work_verified === 'false') {
            return $query;
        }

        return $query->where('borrowers.work_address_verified', '=', \DB::raw(1));
    }

}
