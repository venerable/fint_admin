<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BankInfo
 *
 * @property int $id
 * @property int $borrower_investor
 * @property string $card_account
 * @property string $account_number
 * @property int $bank_id
 * @property string $card_number
 * @property string $cvv
 * @property string $exp_date
 * @property int $exp_yr
 * @property string $address
 * @property string $pin
 * @property \Carbon\Carbon $date_added
 * @property int $owner_id
 * @property int $card_type
 * @property int $primary_card
 * @property string $last_four
 * @property string $paystack_token
 * @property string $paystack_auth
 * @property string $paystack_email
 * @property int $paystack_reusable
 * @property string $fw_token
 * @property string $fw_ref
 *
 * @package App\Models
 */
class BankInfo extends Eloquent
{
    protected $table = 'bank_info';
    public $timestamps = FALSE;

    protected $casts = [
        'borrower_investor' => 'int',
        'bank_id' => 'int',
        'exp_yr' => 'int',
        'owner_id' => 'int',
        'card_type' => 'int',
        'primary_card' => 'int',
        'paystack_reusable' => 'int'
    ];

    protected $dates = [
        'date_added'
    ];

    protected $hidden = [
        'paystack_token',
        'fw_token'
    ];

    protected $fillable = [
        'borrower_investor',
        'card_account',
        'account_number',
        'bank_id',
        'card_number',
        'cvv',
        'exp_date',
        'exp_yr',
        'address',
        'pin',
        'date_added',
        'card_type',
        'primary_card',
        'last_four',
        'paystack_token',
        'paystack_auth',
        'paystack_email',
        'paystack_reusable',
        'fw_token',
        'fw_ref'
    ];

    /**
     * Get the bank associated with this account
     */
    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }
}
