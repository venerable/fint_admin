<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:27 +0000.
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Investor
 *
 * @property int $id
 * @property string $f_name
 * @property string $l_name
 * @property string $m_name
 * @property string $phone
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $bvn
 * @property string $tin
 * @property int $dob_day
 * @property int $dob_month
 * @property int $dob_year
 * @property string $date_of_birth
 * @property int $age
 * @property int $confirmed
 * @property int $representing
 * @property int $gender
 * @property string $transaction_pin
 * @property \Carbon\Carbon $date_joined
 *
 * @package App\Models
 */
class Investor extends Eloquent
{
    public $incrementing = FALSE;
    public $timestamps = FALSE;

    protected $casts = [
        'id' => 'int',
        'dob_day' => 'int',
        'dob_month' => 'int',
        'dob_year' => 'int',
        'age' => 'int',
        'confirmed' => 'int',
        'representing' => 'int',
        'gender' => 'int'
    ];

    protected $dates = [
        'date_joined'
    ];

    protected $hidden = [
        'password'
    ];

    protected $fillable = [
        'f_name',
        'l_name',
        'm_name',
        'phone',
        'email',
        'username',
        'password',
        'bvn',
        'tin',
        'dob_day',
        'dob_month',
        'dob_year',
        'date_of_birth',
        'age',
        'confirmed',
        'representing',
        'gender',
        'transaction_pin',
        'date_joined'
    ];

    /**
     * Get the loan created by the investor.
     */
    public function notes()
    {
        return $this->hasMany('App\Models\Note');
    }

    /**
     * Scoped Queries
     */
    public function scopeId($query, $id)
    {
        if (empty($id)) {
            return $query;
        }

        return $query->where('investors.id', 'LIKE', $id . '%');
    }

    public function scopeFirstName($query, $f_name)
    {
        if (empty($f_name)) {
            return $query;
        }

        return $query->where('investors.f_name', 'LIKE', $f_name . '%');
    }

    public function scopeLastName($query, $l_name)
    {
        if (empty($l_name)) {
            return $query;
        }

        return $query->where('investors.l_name', 'LIKE', $l_name . '%');
    }

    public function scopeUsername($query, $username)
    {
        if (empty($username)) {
            return $query;
        }

        return $query->where('investors.username', 'LIKE', $username . '%');
    }

    public function scopePhone($query, $phone)
    {
        if (empty($phone)) {
            return $query;
        }

        return $query->where('investors.phone', 'LIKE', $phone . '%');
    }

    public function scopeEmail($query, $email)
    {
        if (empty($email)) {
            return $query;
        }

        return $query->where('investors.email', 'LIKE', $email . '%');
    }

    public function scopeDateFrom($query, $dateFrom)
    {
        if (empty($dateFrom)) {
            return $query;
        }

        $dateFrom = date("Y-m-d H:i:s", strtotime(substr($dateFrom,0,10)));

        return $query->where('investors.date_joined', '>=', DB::raw("DATE('$dateFrom')"));
    }

    public function scopeDateTo($query, $dateTo)
    {
        if (empty($dateTo)) {
            return $query;
        }

        $timestamp = null;

        if (strlen($dateTo) == 24) {
            $timestamp = strtotime(substr($dateTo,0,10)) + 60 * 60 * 24;
        } else {
            $timestamp = strtotime(substr($dateTo,0,10));
        }

        $dateTo = date("Y-m-d H:i:s", $timestamp + 60 * 60 * 24);

        return $query->where('investors.date_joined', '<', DB::raw("DATE('$dateTo')"));
    }

    public function scopeHasFundedLoan($query, $funded_loan)
    {
        if (empty($funded_loan)) {
            return $query;
        }

        if ($funded_loan == 'funded_loan') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('investors as i2')
                    ->join('notes', 'notes.investor_id', '=', DB::raw('i2.id'))
                    ->where('investors.id', '=', DB::raw('i2.id'));
            });
        } else {
            return $query->whereNotExists(function ($query) {
                $query->selectRaw(1)
                    ->from('investors as i2')
                    ->join('notes', 'notes.investor_id', '=', DB::raw('i2.id'))
                    ->where('investors.id', '=', DB::raw('i2.id'));
            });
        }

    }

    public function scopePaybackStatus($query, $payback_status)
    {
        if (empty($payback_status)) {
            return $query;
        }

        if ($payback_status == 'success') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('investors as i2')
                    ->join('notes', 'notes.investor_id', '=', DB::raw('i2.id'))
                    ->join('paybacks', 'paybacks.loan_id', '=', DB::raw('notes.loan_id'))
                    ->where('paybacks.success', '=', DB::raw(1))
                    ->where('investors.id', '=', DB::raw('i2.id'));
            });
        } else if ($payback_status == 'pending') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('investors as i2')
                    ->join('notes', 'notes.investor_id', '=', DB::raw('i2.id'))
                    ->join('paybacks', 'paybacks.loan_id', '=', DB::raw('notes.loan_id'))
                    ->where('paybacks.success', '=', DB::raw(0))
                    ->where('paybacks.attempts', '=', DB::raw(0))
                    ->where('investors.id', '=', DB::raw('i2.id'));
            });
        } else if ($payback_status == 'failed') {
            return $query->whereExists(function ($query) {
                $query->selectRaw(1)
                    ->from('investors as i2')
                    ->join('notes', 'notes.investor_id', '=', DB::raw('i2.id'))
                    ->join('paybacks', 'paybacks.loan_id', '=', DB::raw('notes.loan_id'))
                    ->where('paybacks.success', '=', DB::raw(0))
                    ->where('paybacks.attempts', '>', DB::raw(1))
                    ->where('investors.id', '=', DB::raw('i2.id'));
            });
        }

    }
}
