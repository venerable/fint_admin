<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HowDidYouHear
 * 
 * @property int $id
 * @property string $name
 * @property int $amount
 * @property int $borrowers
 * @property int $investors
 *
 * @package App\Models
 */
class HowDidYouHear extends Eloquent
{
	protected $table = 'how_did_you_hear';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'amount' => 'int',
		'borrowers' => 'int',
		'investors' => 'int'
	];

	protected $fillable = [
		'name',
		'amount',
		'borrowers',
		'investors'
	];
}
