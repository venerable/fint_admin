<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class File
 * 
 * @property int $id
 * @property int $file_type
 * @property string $file_title
 * @property \Carbon\Carbon $date_added
 * @property string $url
 * @property string $description
 * @property int $loan_id
 * @property int $show_file
 *
 * @package App\Models
 */
class File extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'file_type' => 'int',
		'loan_id' => 'int',
		'show_file' => 'int'
	];

	protected $dates = [
		'date_added'
	];

	protected $fillable = [
		'file_type',
		'file_title',
		'date_added',
		'url',
		'description',
		'show_file'
	];

    /**
     * Get the loan that uploaded the file.
     */
    public function loan()
    {
        return $this->belongsTo('App\Models\LoanRequest', 'loan_id');
    }

    /**
     * Get the filetype of the file.
     */
    public function filetype()
    {
        return $this->belongsTo('App\Models\FileType', 'file_type');
    }
}
