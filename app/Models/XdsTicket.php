<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class XdsTicket
 * 
 * @property int $id
 * @property string $ticket
 * @property string $time_created
 *
 * @package App\Models
 */
class XdsTicket extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'ticket',
		'time_created'
	];
}
