<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FileType
 * 
 * @property int $id
 * @property string $file_type
 * @property string $description
 *
 * @package App\Models
 */
class FileType extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'file_type',
		'description'
	];

    /**
     * Get the files uploaded by the borrower.
     */
    public function files()
    {
        return $this->hasMany('App\Models\File', 'file_type');
    }
}
