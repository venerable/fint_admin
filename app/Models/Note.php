<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:28 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Note
 * 
 * @property int $id
 * @property int $amount_in
 * @property int $investor_id
 * @property \Carbon\Carbon $date_created
 * @property int $loan_id
 * @property int $total_return
 * @property \Carbon\Carbon $date_removed
 * @property int $status
 *
 * @package App\Models
 */
class Note extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'amount_in' => 'int',
		'investor_id' => 'int',
		'loan_id' => 'int',
		'total_return' => 'int',
		'status' => 'int'
	];

	protected $dates = [
		'date_created',
//		'date_removed'
	];

	protected $fillable = [
	    'id',
        'loan_id',
		'amount_in',
		'investor_id',
		'date_created',
		'total_return',
		'date_removed',
		'status'
	];

    /**
     * Get the lender that owns the note.
     */
    public function lender()
    {
        return $this->belongsTo('App\Models\Investor', 'investor_id');
    }

    /**
     * Get the loan associated with this note.
     */
    public function loan()
    {
        return $this->belongsTo('App\Models\LoanRequest', 'loan_id');
    }

    public static function insertIgnore($data)
    {
        $self = new static();

        $data['date_removed'] = '0000-00-00 00:00:00';

        \DB::insert('INSERT IGNORE INTO ' . $self->getTable() . ' (' . implode(',', array_keys($data)) .
            ') values (?' . str_repeat(',?', count($data) - 1) . ')', array_values($data));
    }
}
