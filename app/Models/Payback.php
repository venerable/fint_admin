<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 03 Jul 2018 09:24:35 +0000.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Payback
 * 
 * @property int $id
 * @property int $loan_id
 * @property int $installment
 * @property int $borrower_id
 * @property int $amount
 * @property string $set_date
 * @property int $attempts
 * @property int $success
 * @property int $employee
 * @property int $percent_paid
 *
 * @package App\Models
 */
class Payback extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'loan_id' => 'int',
		'installment' => 'int',
		'borrower_id' => 'int',
		'amount' => 'int',
		'attempts' => 'int',
		'success' => 'int',
		'employee' => 'int',
		'percent_paid' => 'int'
	];

	protected $fillable = [
		'installment',
		'borrower_id',
		'amount',
		'set_date',
		'attempts',
		'success',
		'employee',
		'percent_paid'
	];

    /**
     * Accessor for set_date attribute
     */
    public function getSetDateAttribute($value)
    {
        return Carbon::createFromTimestamp($value)->toDateString();
    }

    /**
     * Get the loan associated with this payback.
     */
    public function loan()
    {
        return $this->belongsTo('App\Models\LoanRequest', 'loan_id');
    }

    /**
     * Scoped Queries
     */

    public function scopeLoanID($query, $loan_id)
    {
        if (empty($loan_id)) {
            return $query;
        }

        return $query->where('paybacks.loan_id', 'LIKE', $loan_id . '%');
    }

    public function scopeBorrowerID($query, $borrower_id)
    {
        if (empty($borrower_id)) {
            return $query;
        }

        return $query->where('paybacks.borrower_id', 'LIKE', $borrower_id . '%');
    }

    public function scopeInvestorID($query, $investor_id)
    {
        if (empty($investor_id)) {
            return $query;
        }

        return $query->whereExists(function ($query) use ($investor_id) {
            $query->selectRaw(1)
                ->from('notes')
                ->where('notes.investor_id', '=', DB::raw($investor_id))
                ->where('paybacks.loan_id', '=', DB::raw('notes.loan_id'));
        });
    }

    public function scopeFromDate($query, $dateFrom)
    {
        if (empty($dateFrom)) {
            return $query;
        }

        $dateFrom = strtotime($dateFrom);

        return $query->where('paybacks.set_date', '>=', $dateFrom);
    }

    public function scopeToDate($query, $dateTo)
    {
        if (empty($dateTo)) {
            return $query;
        }

        $dateTo = strtotime($dateTo);

        return $query->where('paybacks.set_date', '<=', $dateTo + 60 * 60 * 24);
    }

    public function scopeAttempted($query, $meta)
    {
        if (empty($meta)) {
            return $query;
        }

        if ($meta == 'success') {
            return $query->where('paybacks.success', '=', DB::raw(1));
        } else if ($meta == 'failed') {
            return $query->where('paybacks.attempts', '>', DB::raw(0))
                ->where('paybacks.success', '=', DB::raw(0));
        } else if ($meta == 'pending') {
            return $query->where('paybacks.attempts', '=', DB::raw(0))
                ->where('paybacks.success', '=', DB::raw(0));
        }
    }
}
