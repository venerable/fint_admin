<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Interest
 * 
 * @property int $id
 * @property string $email
 * @property int $type
 * @property \Carbon\Carbon $time_in
 * @property int $sent
 *
 * @package App\Models
 */
class Interest extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'type' => 'int',
		'sent' => 'int'
	];

	protected $dates = [
		'time_in'
	];

	protected $fillable = [
		'email',
		'type',
		'time_in',
		'sent'
	];
}
