<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class State
 * 
 * @property int $state_id
 * @property string $state_name
 * @property int $country_id
 *
 * @package App\Models
 */
class State extends Eloquent
{
	protected $primaryKey = 'state_id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'state_id' => 'int',
		'country_id' => 'int'
	];

	protected $fillable = [
		'state_name',
		'country_id'
	];

    /**
     * Get the cities that belongs to this state.
     */
    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }

    /**
     * Get the borrowers through the city.
     */
    public function borrowersHome()
    {
        return $this->hasManyThrough('App\Models\Borrower', 'App\Models\City',
            'state_id', 'town_id');
    }

    /**
     * Get the borrowers through the city.
     */
    public function borrowersBusiness()
    {
        return $this->hasManyThrough('App\Models\Borrower', 'App\Models\City',
            'state_id', 'b_town_id');
    }


}
