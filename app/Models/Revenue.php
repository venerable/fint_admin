<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Feb 2018 16:20:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Revenue
 * 
 * @property int $id
 * @property float $amount
 * @property string $note
 * @property \Carbon\Carbon $date_created
 * @property int $related_loan
 * @property int $related_note
 * @property int $related_payback
 *
 * @package App\Models
 */
class Revenue extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'amount' => 'float',
		'related_loan' => 'int',
		'related_note' => 'int',
		'related_payback' => 'int'
	];

	protected $dates = [
		'date_created'
	];

	protected $fillable = [
		'amount',
		'note',
		'date_created',
		'related_loan',
		'related_note',
		'related_payback'
	];
}
