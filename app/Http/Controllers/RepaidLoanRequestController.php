<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\LoanRequestTrait;
use App\Models\LoanRequest;
use App\Models\RepaidLoanRequest;
use Illuminate\Http\Request;

class RepaidLoanRequestController extends Controller
{
    use LoanRequestTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RepaidLoanRequest::with(['borrower:id,f_name,l_name,phone,address_verified,work_address_verified',
            'purpose:id,purpose_name',
            'paybacks:id,loan_id,installment,borrower_id,set_date,attempts,success',
            'investments:id,amount_in,investor_id,date_created,loan_id',
            'investments.lender:id,username',
            'files:file_type,file_title,url,loan_id',
            'files.filetype:id,description'])
            ->paginate(5);
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        return RepaidLoanRequest::with(['borrower:id,f_name,l_name,phone,address_verified,work_address_verified',
            'purpose:id,purpose_name',
            'paybacks:id,loan_id,installment,borrower_id,set_date,attempts,success',
            'investments:id,amount_in,investor_id,date_created,loan_id',
            'investments.lender:id,username',
            'files:file_type,file_title,url,loan_id',
            'files.filetype:id,description'])
            ->id($request->loan_id)
            ->borrowerID($request->borrower_id)
            ->loanTitle($request->loan_title)
            ->loanGrade($request->loan_grade)
            ->interestRate($request->interest_rate)
            ->fromAmount(json_decode($request->amount)->from)
            ->toAmount(json_decode($request->amount)->to)
            ->fromDate(json_decode($request->date_range)->from)
            ->toDate(json_decode($request->date_range)->to)
            ->funded($request->funded)
            ->hasFiles($request->has_files)
            ->paginate(50);
    }
}

