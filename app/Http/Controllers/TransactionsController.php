<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Transaction::orderBy('set_date', 'asc')
            ->paginate(50);
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $transactions = Transaction::with('transaction_type')
            ->loanID($request->loan_id)
            ->userID($request->user_id)
            ->profileType($request->profile_type)
            ->transactionType($request->transaction_type)
            ->reference($request->reference)
            ->fromDate(json_decode($request->date_range)->from)
            ->toDate(json_decode($request->date_range)->to)
            ->orderBy('date_created', 'desc')
            ->paginate($request->per_page);

        return $transactions;
    }

}

