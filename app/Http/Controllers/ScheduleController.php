<?php

namespace App\Http\Controllers;

use App\Models\Investor;
use App\Models\LoanRequest;
use App\Models\Note;
use App\Models\Payback;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Payback::orderBy('set_date', 'asc')
            ->paginate(50);
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $paybacks = Payback::loanID($request->loan_id)
            ->borrowerID($request->borrower_id)
            ->investorID($request->lender_id)
            ->fromDate(json_decode($request->date_range)->from)
            ->toDate(json_decode($request->date_range)->to)
            ->attempted($request->attempted)
            ->orderBy('set_date', 'asc')
            ->paginate($request->per_page);

        foreach ($paybacks as $payback) {
            if ($payback->percent_paid < 100) {
                $payback->percent_paid_computed = $payback->percent_paid / 100 * $payback->amount;
            }
        }

        if ($request->lender_id) {
            $loan_id = NULL;
            $loan_term = NULL;
            $total_return = NULL;
            $interest_rate = NULL;

            foreach ($paybacks as $payback) {
                if ($payback->loan_id != $loan_id) {
                    $loan_id = $payback->loan_id;
                    $loan_ = LoanRequest::select('term', 'interest_rate')->where('id', $payback->loan_id)->get();
                    $loan_term = $loan_[0]->term;
                    $interest_rate = $loan_[0]->interest_rate;
                    $total_return = Note::select('total_return')->where([
                        ['loan_id', '=', $payback->loan_id],
                        ['investor_id', '=', $request->lender_id]
                    ])->get();
                    $total_return = $total_return[0]->total_return;
                }

                $payback->amount = $one_return = $total_return / $loan_term;

                $pay_equ        = (1 - (0.015 / (1 + ($interest_rate / 100))));
                $payback->lender_payable   = $this->roundNum((($one_return * $pay_equ) - 154), .50);

            }
        }

        return $paybacks;
    }


    /**
     * Misc
     */

    function roundNum($num, $nearest) {
        return round($num / $nearest) * $nearest;
    }

}

