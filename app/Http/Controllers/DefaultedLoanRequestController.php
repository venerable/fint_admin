<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\LoanRequestTrait;
use App\Models\ActivityLog;
use App\Models\DefaultInfo;
use App\Models\Defaultt;
use App\Models\LoanRequest;
use App\Models\Note;
use App\Models\Payback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DefaultedLoanRequestController extends Controller
{
    use LoanRequestTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Defaultt::with(['borrower:id,f_name,l_name,phone,address_verified,work_address_verified',
            'purpose:id,purpose_name',
            'paybacks:id,loan_id,installment,borrower_id,set_date,attempts,success',
            'investments:id,amount_in,investor_id,date_created,loan_id',
            'investments.lender:id,username',
            'files:file_type,file_title,url,loan_id',
            'files.filetype:id,description'])
            ->paginate(5);
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        return Defaultt::with(['borrower:id,f_name,l_name,phone,address_verified,work_address_verified',
            'purpose:id,purpose_name',
            'paybacks:id,loan_id,installment,borrower_id,set_date,attempts,success',
            'investments:id,amount_in,investor_id,date_created,loan_id',
            'investments.lender:id,username',
            'files:file_type,file_title,url,loan_id',
            'files.filetype:id,description'])
            ->id($request->loan_id)
            ->borrowerID($request->borrower_id)
            ->loanTitle($request->loan_title)
            ->loanGrade($request->loan_grade)
            ->interestRate($request->interest_rate)
            ->fromAmount(json_decode($request->amount)->from)
            ->toAmount(json_decode($request->amount)->to)
            ->fromDate(json_decode($request->date_range)->from)
            ->toDate(json_decode($request->date_range)->to)
            ->funded($request->funded)
            ->hasFiles($request->has_files)
            ->paginate(50);
    }

    /**
     * Move a loan out of default
     */
    public function moveOutOfDefault(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $loan = Defaultt::find($request->loan_id);
                // move loan to defaults table
                LoanRequest::create($loan->toArray());

                //log
                ActivityLog::create([
                    "user_id" => $loan->borrower,
                    "type" => 0,
                    "action" => "Loan Reverted",
                    "related_loan" => $loan->id,
                    "date" => \Carbon\Carbon::now()
                ]);

                // move notes to default_notes table and log
                $notes = $loan->investments;
                if (count($notes)) {
                    foreach ($notes as $note) {
                        Note::create($note->toArray());
                        ActivityLog::create([
                            "user_id" => $note->investor_id,
                            "type" => 1,
                            "action" => "Note Reverted",
                            "related_loan" => $loan->id,
                            "date" => \Carbon\Carbon::now()
                        ]);

                        $note->delete();
                    }
                }

                // move paybacks to default_paybacks table and log
                $paybacks = $loan->paybacks;
                if (count($paybacks)) {
                    foreach ($paybacks as $payback) {
                        Payback::create($payback->toArray());
                        ActivityLog::create([
                            "user_id" => $payback->borrower_id,
                            "type" => 0,
                            "action" => "Payback Reverted",
                            "related_loan" => $loan->id,
                            "date" => \Carbon\Carbon::now()
                        ]);

                        $payback->delete();
                    }
                }

                // create Defaulted Loan Info
                DefaultInfo::create([
                    "loan_id" => $loan->id,
                    "status" => 0,
                    "notes" => $request->remark,
                ]);

                $loan->delete();
            });

            return 'true';
        } catch (\Exception $e) {
            return $e->getMessage();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

}

