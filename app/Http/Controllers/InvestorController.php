<?php

namespace App\Http\Controllers;

use App\Http\Utils\EmailUtil;
use App\Http\Utils\SMSUtil;
use App\Models\Investor;
use Illuminate\Http\Request;

class InvestorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Investor::with('notes')->paginate(50);
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        return Investor::with('notes')
            ->id($request->lender_id)
            ->firstName($request->f_name)
            ->lastName($request->l_name)
            ->username($request->username)
            ->email($request->email)
            ->phone($request->phone)
            ->dateFrom(json_decode($request->date_range)->from)
            ->dateTo(json_decode($request->date_range)->to)
            ->hasFundedLoan($request->has_funded_loan)
            ->paybackStatus($request->payback_status)
            ->paginate($request->per_page);
    }



    public function sendMail(Request $request)
    {
        $response = EmailUtil::boot()->sendNotificationMail(
            $request->email,
            $request->subject,
            array($request->f_name, $request->message, $request->subject)
        );
        $response = (array)$response;

        return ['status' => $response];
    }

    public function sendSMS(Request $request)
    {
        $response = SMSUtil::sendSms(
            $request->f_name,
            $request->message,
            $request->phone
        );

        $response = json_decode($response, TRUE);

        if ($response['status'] !== 'OK') {
            return ['status' => FALSE];
        }

        return ['status' => TRUE];
    }

}

