<?php

namespace App\Http\Controllers;

use App\Http\Utils\EmailUtil;
use App\Http\Utils\SMSUtil;
use App\Models\ActivityLog;
use App\Models\AdminComment;
use App\Models\Borrower;
use App\Models\BvnVerificationRecord;
use Illuminate\Http\Request;

class BorrowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Borrower::with(['requestedLoan:id,borrower,term,interest_rate,loan_grade,amount,active,date_created,funding_time_elapsed',
            'homeCity:city_id,city_name',
            'businessCity:city_id,city_name',
            'loanData:borrower_id,locative,cash_inflow,cash_outflow',
            'bvnDetails:borrower,f_name,l_name,dob,phone',
            'commentInfo:id,admin_id,comment,entity_id,date_created',
            'commentInfo.admin:id,username'])
            ->paginate(50);
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        return Borrower::with(['requestedLoan:id,borrower,term,interest_rate,loan_grade,amount,active,date_created,funding_time_elapsed',
            'homeCity:city_id,city_name',
            'businessCity:city_id,city_name',
            'loanData:borrower_id,locative,cash_inflow,cash_outflow',
            'bvnDetails:borrower,f_name,l_name,dob,phone',
            'commentInfo:id,admin_id,comment,entity_id,date_created',
            'commentInfo.admin:id,username',
            'accounts:id,owner_id,bank_id,account_number',
            'accounts.bank:id,bank_name'])
            ->id($request->borrower_id)
            ->firstName($request->f_name)
            ->lastName($request->l_name)
            ->username($request->username)
            ->email($request->email)
            ->phone($request->phone)
            ->dateFrom(json_decode($request->date_range)->from)
            ->dateTo(json_decode($request->date_range)->to)
            ->hasTakenTest($request->risk_assessment)
            ->hasEmployment($request->employment_status)
            ->hasActiveLoan($request->active_loan)
            ->hasFiles($request->has_files)
            ->IsBvnVerified($request->bvn_verified)
            ->IsPhoneVerified($request->phone_verified)
            ->IsHomeVerified($request->home_verified)
            ->IsWorkVerified($request->work_verified)
            ->paginate($request->per_page);
    }

    /**
     * Verify a borrower
     */
    public function verify(Request $request)
    {
        $borrower = Borrower::find($request->id);

        if ($request->field == 'bvn') {
            $borrower->bvn_verified = 1;
        } else if ($request->field == 'phone') {
            $borrower->phone_verified = 1;
        }

        //log
        ActivityLog::create([
            "user_id" => $request->id,
            "type" => 0,
            "action" => ucfirst($request->field) . " Cleared",
            "related_loan" => 0,
            "date" => \Carbon\Carbon::now()
        ]);

        return $borrower->save() ? 'true' : 'false';
    }

    /**
     * Invalidate a borrower
     */
    public function unverify(Request $request)
    {
        $borrower = Borrower::find($request->id);

        if ($request->field == 'bvn') {
            $borrower->bvn_verified = 0;
        } else if ($request->field == 'phone') {
            $borrower->phone_verified = 0;
        }

        //log
        ActivityLog::create([
            "user_id" => $request->id,
            "type" => 0,
            "action" => ucfirst($request->field) . " Uncleared",
            "related_loan" => 0,
            "date" => \Carbon\Carbon::now()
        ]);

        return $borrower->save() ? 'true' : 'false';
    }

    public function resetBvnVerifyStatus(Request $request)
    {
        $record = BvnVerificationRecord::where('borrower', $request->id);

        if ($record->count()) {
            try {
                \DB::transaction(function () use ($request, $record) {
                    $record->update([
                        'attempt' => 0,
                        'date_queried' => NULL
                    ]);
                    //log
                    ActivityLog::create([
                        "user_id" => $request->id,
                        "type" => 0,
                        "action" => "Bvn status resetted",
                        "related_loan" => 0,
                        "date" => \Carbon\Carbon::now()
                    ]);
                });

                return ['status' => TRUE];
            } catch (\Exception $e) {
                return $e->getMessage();
            } catch (\Throwable $e) {
                return $e->getMessage();
            }
        } else {
            return ['status' => FALSE, 'desc' => 'NO_RECORD'];
        }
    }

    public function sendMail(Request $request)
    {
        $response = EmailUtil::boot()->sendNotificationMail(
            $request->email,
            $request->subject,
            array($request->f_name, $request->message, $request->subject)
        );
        $response = (array)$response;

        return ['status' => $response];
    }

    public function sendSMS(Request $request)
    {
        $response = SMSUtil::sendSms(
            $request->f_name,
            $request->message,
            $request->phone
        );

        $response = json_decode($response, TRUE);

        if ($response['status'] !== 'OK') {
            return ['status' => FALSE];
        }

        return ['status' => TRUE];
    }

    public function editComment(Request $request)
    {
        $admin_id = $request->admin_id;
        $username_ = NULL;

        if (!$admin_id) {
            $admin = \Auth::user();
            $admin_id = $admin->id;
            $username_ = $admin->username;
        }

        $comment = AdminComment::updateOrCreate(
            ['admin_id' => $admin_id, 'entity_id' => $request->entity_id, 'entity' => 'BORROWER'],
            ['comment' => $request->comment]
        );

        return ['status' => TRUE, 'username' => $username_, 'id' => $comment->id];
    }

    public function deleteComment(Request $request)
    {
        $comment = AdminComment::find($request->comment_id);

        $comment->delete();

        return ['status' => TRUE];
    }

    public function editDetails(Request $request)
    {
        $borrower = Borrower::find($request->borrower_id);

        if ($request->field == 'email')
            $borrower->email = $request->data;

        $borrower->save();

        return ['status' => TRUE];
    }
}

