<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\LoanRequestTrait;
use App\Http\Utils\EmailUtil;
use App\Http\Utils\SMSUtil;
use App\Models\ActivityLog;
use App\Models\Borrower;
use App\Models\BorrowerLoanDatum;
use App\Models\CanceledLoan;
use App\Models\DefaultInfo;
use App\Models\DefaultNote;
use App\Models\DefaultPayback;
use App\Models\Defaultt;
use App\Models\Investor;
use App\Models\LoanRequest;
use App\Models\Note;
use App\Models\Notification;
use App\Models\Payback;
use App\Models\RepaidLoanRequest;
use App\Models\RepaidNote;
use App\Models\ToBePaid;
use App\Models\Transaction;
use App\Models\VoidedNote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Psy\Util\Json;

class LoanRequestController extends Controller
{
    use LoanRequestTrait;


    public function index()
    {
        return LoanRequest::with(['borrower:id,f_name,l_name,phone,email,address_verified,work_address_verified',
            'purpose:id,purpose_name',
            'paybacks:id,loan_id,installment,borrower_id,set_date,attempts,success,percent_paid',
            'investments:id,amount_in,investor_id,date_created,loan_id',
            'investments.lender:id,username',
            'files:file_type,file_title,url,loan_id',
            'files.filetype:id,description'])
            ->paginate(5);
    }

    /**
     * Display a filtered listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        return LoanRequest::with(['borrower:id,f_name,l_name,phone,email,address_verified,work_address_verified',
            'purpose:id,purpose_name',
            'paybacks:id,loan_id,installment,borrower_id,set_date,attempts,success,percent_paid',
            'investments:id,amount_in,investor_id,date_created,loan_id',
            'investments.lender:id,username',
            'files:file_type,file_title,url,loan_id',
            'files.filetype:id,description'])
            ->id($request->loan_id)
            ->borrowerID($request->borrower_id)
            ->investorID($request->investor_id)
            ->loanTitle($request->loan_title)
            ->loanGrade($request->loan_grade)
            ->interestRate($request->interest_rate)
            ->loanTerm($request->loan_term)
            ->fromAmount(json_decode($request->amount)->from)
            ->toAmount(json_decode($request->amount)->to)
            ->fromDate(json_decode($request->date_range)->from)
            ->toDate(json_decode($request->date_range)->to)
            ->funded($request->funded)
            ->hasFiles($request->has_files)
            ->IsBvnVerified($request->bvn_verified)
            ->IsPhoneVerified($request->phone_verified)
            ->IsHomeVerified($request->home_verified)
            ->IsWorkVerified($request->work_verified)
            ->IsBankStatementVerified($request->bank_stat_verified)
            ->paginate($request->per_page);
    }

    /**
     * Verify a borrower
     */
    public function verify(Request $request)
    {
        $loan = LoanRequest::find($request->loan_id);
        $borrower = Borrower::find($request->borrower_id);
        $status = FALSE;

        if ($request->field == 'home') {
            $borrower->address_verified = 1;
            $status = $borrower->save();
        } else if ($request->field == 'office') {
            $borrower->work_address_verified = 1;
            $status = $borrower->save();
        } else if ($request->field == 'bank_statement') {
            $loan->bank_stat_verified = 1;
            $status = $loan->save();
        }
        //log
        ActivityLog::create([
            "user_id" => $request->borrower_id,
            "type" => 0,
            "action" => ucfirst(str_replace('_', ' ', $request->field)) . " Cleared",
            "related_loan" => $request->loan_id,
            "date" => \Carbon\Carbon::now()
        ]);

        if ($borrower->address_verified == 1 && $borrower->work_address_verified == 1 && $loan->bank_stat_verified == 1) {
            $loan->disb_clear = 1;
            $status = $loan->save();
            //log
            ActivityLog::create([
                "user_id" => $request->borrower_id,
                "type" => 0,
                "action" => 'Loan Cleared',
                "related_loan" => $request->loan_id,
                "date" => \Carbon\Carbon::now()
            ]);
        }

        return $status ? 'true' : 'false';
    }

    /**
     * Invalidate a borrower
     */
    public function unverify(Request $request)
    {
        $loan = LoanRequest::find($request->loan_id);
        $borrower = Borrower::find($request->borrower_id);
        $status = FALSE;

        if ($request->field == 'home') {
            $borrower->address_verified = 0;
            $status = $borrower->save();
        } else if ($request->field == 'office') {
            $borrower->work_address_verified = 0;
            $status = $borrower->save();
        } else if ($request->field == 'bank_statement') {
            $loan->bank_stat_verified = 0;
            $status = $loan->save();
        }
        //log
        ActivityLog::create([
            "user_id" => $request->borrower_id,
            "type" => 0,
            "action" => ucfirst(str_replace('_', ' ', $request->field)) . " Uncleared",
            "related_loan" => $request->loan_id,
            "date" => \Carbon\Carbon::now()
        ]);

        // For disapproving for disbursement
        if ($loan->disbursed == 0) {
            $loan->disb_clear = 0;
            $status = $loan->save();
            //log
            ActivityLog::create([
                "user_id" => $request->borrower_id,
                "type" => 0,
                "action" => 'Loan Uncleared',
                "related_loan" => $request->loan_id,
                "date" => \Carbon\Carbon::now()
            ]);
        }

        return $status ? 'true' : 'false';
    }

    /**
     * Clear a loan for disbursement
     */
    public function clear(Request $request)
    {
        $loan = LoanRequest::find($request->loan_id);

        if ($request->action == 'clear') {
            $loan->disb_clear = 1;
        } else {
            $loan->disb_clear = 0;
        }

        //log
        ActivityLog::create([
            "user_id" => $loan->borrower,
            "type" => 0,
            "action" => 'Loan ' . ucfirst($request->action),
            "related_loan" => $request->loan_id,
            "date" => \Carbon\Carbon::now()
        ]);

        return $loan->save() ? 'true' : 'false';
    }

    /**
     * Cancel a loan for disbursement
     */
    public function cancel(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $loan = LoanRequest::find($request->loan_id);
                // move loan to canceled_loans table
                CanceledLoan::insertIgnore($loan->toArray());

                //log
                ActivityLog::create([
                    "user_id" => $loan->borrower,
                    "type" => 0,
                    "action" => "Loan Cancelled",
                    "related_loan" => $loan->id,
                    "date" => \Carbon\Carbon::now()
                ]);

                // move notes to voided_notes table and log
                $notes = $loan->investments;
                if (count($notes)) {
                    foreach ($notes as $note) {
                        VoidedNote::insertIgnore($note->toArray());
                        ToBePaid::insertIgnore([
                            "for_meta" => "voided_note",
                            "amount" => $note->amount_in,
                            "to_who" => $note->investor_id,
                            "related_loan" => $note->loan_id,
                            "related_note" => $note->id
                        ]);
                        ActivityLog::create([
                            "user_id" => $note->investor_id,
                            "type" => 1,
                            "action" => "Voided Note",
                            "related_loan" => $loan->id,
                            "date" => \Carbon\Carbon::now()
                        ]);

                        $note->delete();
                    }
                }

                $loan->delete();
            });

            return 'true';
        } catch (\Exception $e) {
            return $e->getMessage();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }

    }

    /**
     * Mark a loan as default
     */
    public function markAsDefault(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $loan = LoanRequest::find($request->loan_id);
                // move loan to defaults table
                Defaultt::insertIgnore($loan->toArray());

                //log
                ActivityLog::create([
                    "user_id" => $loan->borrower,
                    "type" => 0,
                    "action" => "Defaulted Loan",
                    "related_loan" => $loan->id,
                    "date" => \Carbon\Carbon::now()
                ]);

                // move notes to default_notes table and log
                $notes = $loan->investments;
                if (count($notes)) {
                    foreach ($notes as $note) {
                        DefaultNote::insertIgnore($note->toArray());
                        ActivityLog::create([
                            "user_id" => $note->investor_id,
                            "type" => 1,
                            "action" => "Defaulted Note",
                            "related_loan" => $loan->id,
                            "date" => \Carbon\Carbon::now()
                        ]);

                        $note->delete();
                    }
                }

                // move paybacks to default_paybacks table and log
                $paybacks = $loan->paybacks;
                if (count($paybacks)) {
                    foreach ($paybacks as $payback) {
                        DefaultPayback::create($payback->toArray());
                        ActivityLog::create([
                            "user_id" => $payback->borrower_id,
                            "type" => 0,
                            "action" => "Defaulted Payback",
                            "related_loan" => $loan->id,
                            "date" => \Carbon\Carbon::now()
                        ]);

                        $payback->delete();
                    }
                }

                // create Defaulted Loan Info
                DefaultInfo::create([
                    "loan_id" => $loan->id,
                    "status" => 0,
                    "notes" => $request->remark,
                ]);

                $loan->delete();
            });

            return 'true';
        } catch (\Exception $e) {
            return $e->getMessage();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }

    }

    /**
     * Fund a loan
     */
    public function fundLoan(Request $request)
    {
        $loan_info = LoanRequest::where('id', $request->loan_id)
            ->select('borrower', 'interest_rate', 'title', 'term', 'funded_amount', 'amount', 'number_of_investors')
            ->get()->toArray();

        $validator = Validator::make($request->all(), [
            'amount' => [
                'bail',
                'required',
                'numeric',
                'min:20000',
                'max:' . (intval($loan_info[0]['amount']) - intval($loan_info[0]['funded_amount'])),
                function ($attribute, $value, $fail) {
                    if (intval($value) % 20000 != 0) {
                        return $fail($attribute . ' should be a factor of NGN 20,000');
                    }
                },
            ],
            'lender_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return ['status' => FALSE, 'errors' => $errors->toJson()];
        }

        try {
            DB::transaction(function () use ($request, $loan_info) {
                // insert a Note
                Note::insertIgnore([
                    'amount_in' => $request->amount,
                    'investor_id' => $request->lender_id,
                    'date_created' => \Carbon\Carbon::now(),
                    'loan_id' => $request->loan_id,
                    'total_return' => round(($loan_info[0]['interest_rate'] / 100.0) * intval($request->amount))
                        + intval($request->amount)
                ]);
                // increment funded amount in LoanRequest
                LoanRequest::where('id', $request->loan_id)->update([
                    'number_of_investors' => intval($loan_info[0]['number_of_investors']) + 1,
                    'funded_amount' => intval($loan_info[0]['funded_amount']) + intval($request->amount)
                ]);
                // insert a Transaction
                Transaction::create([
                    "transaction_type" => 1,
                    "from" => $request->lender_id,
                    'to' => 0,
                    'payback_id' => 0,
                    'description' => 'Note Pledge of N' . $request->amount . ' for Loan #' . $request->loan_id,
                    'amount' => $request->amount,
                    'profile' => 1,
                    'user_id' => $request->lender_id,
                    'confirmation' => 'FINT_MANUAL',
                    'related_loan' => $request->loan_id,
                    'refunded' => 0,
                ]);
                //insert a Log
                ActivityLog::create([
                    "user_id" => $request->lender_id,
                    "type" => 1,
                    "action" => 'Successfully Invested In Loan # ' . $request->loan_id,
                    "related_loan" => $request->loan_id,
                    "date" => \Carbon\Carbon::now()
                ]);

                /****************** Notifications ***********************/
                $lender_info = Investor::where('id', $request->lender_id)
                    ->select('email', 'f_name', 'phone')->get()->toArray();
                $borrower_info = Borrower::where('id', $loan_info[0]['borrower'])
                    ->select('email', 'f_name', 'phone')->get()->toArray();
                $percentage_funded = round(intval($loan_info[0]['funded_amount']) / intval($loan_info[0]['amount']) * 100);

                // Investor
                $body = 'You Have Successfully Invested in Loan #' . $request->loan_id;
                EmailUtil::boot()->sendNotificationwithAttachmentMail(
                    $lender_info[0]['email'],
                    'Congratulations! We have received your investment in Loan #' . $request->loan_id,
                    array(
                        $body,
                        'NGN',
                        $request->amount,
                        array(
                            array('Reference', 'FINT_MANUAL'),
                            array('Title', $loan_info[0]['title']),
                            array('Card', '-'),
                            array('Date', date('j-m-Y'))
                        ))
                );
                SMSUtil::sendSms($lender_info[0]['f_name'], $body, $lender_info[0]['phone']);

                // Borrower
                if ($percentage_funded < 80) {
                    $body = 'Your Loan #' . $request->loan_id . ' has received a new investment.';
                    EmailUtil::boot()->sendNotificationWithAttachmentMail(
                        $borrower_info[0]['email'],
                        'Congratulations! You have received an investment in Loan #' . $request->loan_id,
                        array(
                            $body,
                            'NGN',
                            $request->amount,
                            array(
                                array('Title', $loan_info[0]['title']),
                                array('Funded', $percentage_funded . ' %'),
                                array('Date', date('j-m-Y'))
                            ))
                    );
                    SMSUtil::sendSms($borrower_info[0]['f_name'], $body, $borrower_info[0]['phone']);
                } else if ($percentage_funded >= 80 && $percentage_funded < 100) {
                    $body = 'You are almost there! Your Loan #' . $request->loan_id .
                        ' has received a new investment and has now reached ' . $percentage_funded . '% funding.';
                    EmailUtil::boot()->sendNotificationWithAttachmentMail(
                        $borrower_info[0]['email'],
                        'You are almost there! Loan #' . $request->loan_id,
                        array(
                            $body,
                            'NGN',
                            $request->amount,
                            array(
                                array('Title', $loan_info[0]['title']),
                                array('Funded', $percentage_funded . ' %'),
                                array('Date', date('j-m-Y'))
                            ))
                    );
                    SMSUtil::sendSms($borrower_info[0]['f_name'], $body, $borrower_info[0]['phone']);
                } else if ($percentage_funded == 100) {
                    $body = 'Your Loan Request, ' . $loan_info[0]['title'] . ', has reached full funding. Head over to FINT and disburse the loan when ready. It will take 2-5 business days for the loan to reach your account after disbursement. Please note that a 8% commitment fee will be charged in order to disburse the loan.<br><br>Loan Amount : ' . $loan_info[0]['amount'] . '<br>Interest Rate : ' . $loan_info[0]['interest_rate'] . '<br>Loan Term : ' . $loan_info[0]['term'] . '<br><br>Your first installment will be determined upon disbursement.';
                    EmailUtil::boot()->sendNotificationMail(
                        $borrower_info[0]['email'],
                        'Congratulations! Your Loan is fully funded',
                        array($borrower_info[0]['f_name'], $body, 'You Made It!')
                    );
                    SMSUtil::sendSms(
                        $borrower_info[0]['f_name'],
                        str_replace('<br>', '\n', $body),
                        $borrower_info[0]['phone']
                    );
                }

            });

            /***************** Response **********************/
            $response = Note::with([
                'lender:id,username'
            ])->where('loan_id', $request->loan_id)->get();

            return ['status' => TRUE, 'lenders' => $response];

        } catch (\Exception $e) {
            return $e->getMessage();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }

    }

    /**
     * Confirm payback
     */
    public function confirmPayback(Request $request)
    {
        $loan_info = LoanRequest::where('id', $request->loan_id)
            ->select('borrower', 'interest_rate', 'title', 'term', 'payback_due', 'payback_amount', 'amount', 'number_of_investors')
            ->get()->toArray();
        $payback_info = Payback::where([
            ['loan_id', '=', $request->loan_id],
            ['success', '=', 0]])
            ->select('id', 'installment', 'amount', 'attempts')
            ->orderBy('installment', 'asc')
            ->limit(1)
            ->get()->toArray();

        try {
            DB::transaction(function () use ($request, $loan_info, $payback_info) {
                // insert a Transaction
                Transaction::create([
                    "transaction_type" => 3,
                    "from" => $loan_info[0]['borrower'],
                    'to' => 0,
                    'payback_id' => $payback_info[0]['id'],
                    'description' => 'Payback #' . $payback_info[0]['installment'] . ' for Loan #' . $request->loan_id,
                    'amount' => $request->amount,
                    'profile' => 0,
                    'user_id' => $loan_info[0]['borrower'],
                    'confirmation' => 'FINT_MANUAL',
                    'related_loan' => $request->loan_id,
                    'refunded' => 0,
                ]);
                // update the Payback
                Payback::where('id', $payback_info[0]['id'])->update([
                    'success' => DB::raw(1),
                    'attempts' => DB::raw(intval($payback_info[0]['attempts']) + 1),
                    'percent_paid' => DB::raw(100)
                ]);
                // update the Loan Request
                LoanRequest::where('id', $request->loan_id)->update([
                    'payback_amount' => intval($loan_info[0]['payback_amount']) + intval($payback_info[0]['amount']),
                ]);
                $loan_info[0]['payback_amount'] = intval($loan_info[0]['payback_amount']) + intval($payback_info[0]['amount']);

                /*******************************/
                $loan = LoanRequest::find($request->loan_id);

                $borrower_info = Borrower::where('id', $loan_info[0]['borrower'])
                    ->select('email', 'f_name', 'phone')->get()->toArray();
                /*******************************/

                // if loan is fully repaid
                if (intval($loan_info[0]['payback_amount']) >= intval($loan_info[0]['payback_due'])) {
                    // move loan to Repaid Loans table
                    RepaidLoanRequest::insertIgnore($loan->toArray());
                    // create Notifications
                    Notification::create([
                        'Executor' => 0,
                        'ExecutorType' => -1,
                        'Receiver' => $loan_info[0]['borrower'],
                        'ReceiverType' => 0,
                        'NotType' => 6,
                        'NotString' => 'Congratulations! The Loan : ' . $loan_info[0]['title'] .
                            ' has been repaid in full! Thank You For Using FINT !',
                        'RelatedNote' => 0,
                        'RelatedLoan' => $request->loan_id
                    ]);

                    $notes = $loan->investments;
                    foreach ($notes as $note) {
                        RepaidNote::insertIgnore($note->toArray());
                        Notification::create([
                            'Executor' => 0,
                            'ExecutorType' => -1,
                            'Receiver' => $note->investor_id,
                            'ReceiverType' => 1,
                            'NotType' => 7,
                            'NotString' => 'Congratulations! The Loan : ' . $loan_info[0]['title'] .
                                ' has been repaid in full! Try investing on FINT again !',
                            'RelatedNote' => $note->id,
                            'RelatedLoan' => $request->loan_id
                        ]);

                        $note->delete();
                    }

                    $loan->delete();
                }

                $notes = $loan->investments;
                // insert to_be_paid
                foreach ($notes as $note) {
                    $this_payment = round(intval($note->total_return) / intval($loan_info[0]['term']));
                    ToBePaid::insertIgnore([
                        "for_meta" => "loan_repayment",
                        "amount" => $this_payment,
                        "to_who" => $note->investor_id,
                        "related_loan" => $note->loan_id,
                        "related_note" => $note->id
                    ]);
                    Notification::create([
                        'Executor' => $loan_info[0]['borrower'],
                        'ExecutorType' => 0,
                        'Receiver' => $note->investor_id,
                        'ReceiverType' => 1,
                        'NotType' => 5,
                        'NotString' => 'Your Monthly Payment for The Loan : ' . $loan_info[0]['title'] .
                            ' has been made! You will be receiving NGN' . $this_payment . ' in 2-5 business days.',
                        'RelatedNote' => $note->id,
                        'RelatedLoan' => $request->loan_id
                    ]);
                    // Send Notifications to Investor.
                    $lender_info = Investor::where('id', $note->investor_id)
                        ->select('email', 'f_name', 'phone')->get()->toArray();
                    $body = 'The Borrower of the Loan, ' . $loan_info[0]['title'] .
                        ', has paid back returns. In 2-5 Business Days, You will receive N' . $this_payment;
                    EmailUtil::boot()->sendNotificationwithAttachmentMail(
                        $lender_info[0]['email'],
                        'Congratulations! You have been paid investment returns on Loan #' . $request->loan_id,
                        array(
                            $body,
                            'NGN',
                            $this_payment,
                            array(
                                array('Reference', 'FINT_MANUAL'),
                                array('Title', $loan_info[0]['title']),
                                array('Card', '-'),
                                array('Date', date('j-m-Y'))
                            ))
                    );
                    SMSUtil::sendSms($lender_info[0]['f_name'], $body, $lender_info[0]['phone']);
                }

                // Send Notifications to borrower.
                EmailUtil::boot()->sendNotificationwithAttachmentMail(
                    $borrower_info[0]['email'],
                    'Congratulations! You Have Paid Installment #' . $payback_info[0]['installment'] . ' of the Loan #' . $request->loan_id,
                    array(
                        'You Have Successfully paid back Installment #' . $payback_info[0]['installment'] . ' to the loan #' . $request->loan_id,
                        'NGN',
                        $request->amount,
                        array(
                            array('Reference', 'FINT_MANUAL'),
                            array('Title', $loan_info[0]['title']),
                            array('Card', '-'),
                            array('Date', date('j-m-Y'))
                        ))
                );
                SMSUtil::sendSms($borrower_info[0]['f_name'],
                    'You have successfully paid back ' . $payback_info[0]['installment'] . ' installment(s) to the loan #' . $request->loan_id,
                    $borrower_info[0]['phone']);

            });

            /***************** Response **********************/
            return ['status' => TRUE];

        } catch (\Exception $e) {
            return $e->getMessage();
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * Fetch a loan's proposal
     */
    public function fetchTestAnswers(Request $request)
    {
        $loan_data = BorrowerLoanDatum::where('borrower_id', $request->borrower_id)->latest('loan_data_id')->get();

        if ($loan_data->isNotEmpty()) {
            $loan_data = $loan_data[0];

            return [
                'Marital Status' => $loan_data->maritial_status,
                'Living Situation' => $loan_data['locative'],
                'Number of Dependants' => $loan_data->dependants,
                'Occupation' => $loan_data->occupation,
                'Industry' => $loan_data->industry,
                'Level of Education' => $loan_data->education,
                'Years in Current Employment' => $loan_data->current_employer,
                'Years in Prior Employment' => $loan_data->prior_employer,
                'Length of Account' => $loan_data->length_of_account,
                'Cash Inflow' => $loan_data->cash_inflow,
                'Cash Outflow' => $loan_data->cash_outflow
            ];
        } else {
            return [];
        }
    }

}

