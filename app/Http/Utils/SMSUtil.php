<?php
/**
 * Created by PhpStorm.
 * User: kraneum
 * Date: 1/9/18
 * Time: 1:45 PM
 */

namespace App\Http\Utils;

class SMSUtil {

    public function __construct() {
        //construct
    }

    /**
     * @param $name
     * @param $message
     * @param $number
     * @param $country_code
     * @return mixed
     */
    public static function sendSms($name, $message, $number, $country_code=234) {

        $phon = substr($number, 1);
        $response = self::carryOutSMS('info@kraneum.com', 'krAFRICA00', 'FINT', $country_code.$phon,
            "Hi ".$name.", \n\n".$message." \n\nBetter Rates, Better Returns. \nFINT TEAM!", 0);
        return $response;
    }

    /**
     * @param $username
     * @param $password
     * @param $sender
     * @param $rec
     * @param $message
     * @param $message_id
     * @return mixed
     */
    public static function carryOutSMS($username, $password, $sender, $rec, $message, $message_id) {
        $parameters = sprintf ( "username=%s&password=%s&sender=%s&mobiles=%s&message=%s", urlencode ( $username ), urlencode ( $password ), urlencode ( $sender ), urlencode ( $rec ), urlencode ( $message ) );
        $responseString = self::sendSMS2('http://portal.bulksmsnigeria.net/api/', $parameters);
        return $responseString;
    }

    /**
     * @param $apiurl
     * @param $params
     * @return mixed
     */
    public static function sendSMS2($apiurl, $params) {

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $apiurl );
        curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
        $response = curl_exec ( $ch );
        return $response;
    }

}