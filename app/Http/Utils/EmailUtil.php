<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13-Apr-18
 * Time: 10:46 AM
 */

namespace App\Http\Utils;

use App\Http\Utils\Traits\GenerateReceipt;

class EmailUtil
{
    use GenerateReceipt;

    private static $from, $sendgrid;

    public static function boot() {
        $self = new EmailUtil();
        $self::$from = new \SendGrid\Email("FINT", "support@fint.ng");
        $self::$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
        return $self;
    }

    public function sendNotificationMail($to, $subject, $content = null)
    {
        $to = new \SendGrid\Email(NULL, $to);

        $message = file_get_contents(__DIR__ . '/views/emails/inform_email_template.html');
        $message = str_replace('{username}', $content[0], $message);
        $message = str_replace('{message}', $content[1], $message);
        $message = str_replace('{header}', $content[2], $message);

        $body = new \SendGrid\Content("text/html", $message);
        $mail = new \SendGrid\Mail(self::$from, $subject, $to, $body);

        $response = self::$sendgrid->client->mail()->send()->post($mail);

        return $response;
    }

    public function sendNotificationWithAttachmentMail($to, $subject, $content = null)
    {
        $to = new \SendGrid\Email(NULL, $to);

        $message = file_get_contents(__DIR__ . '/views/emails/inform_email_with_attachment_template.html');

        $intro   = $content[0];
        $curr    = $content[1];
        $amt     = $content[2];
        $deets   = $content[3];

        $message = str_replace('{intro}', $intro, $message);
        $message = str_replace('{curr}', $curr, $message);
        $message = str_replace('{amt}', $amt, $message);

        for ($i = 0; $i < count($deets); $i++) {
            $row   = $deets[$i];
            $deet  = $row[0];
            $value = $row[1];

            $message = str_replace('{deet}', $deet, $message);
            $message = str_replace('{value}', $value, $message);
        }

        $file_name = "".time().".pdf";
        $this->generateReceipt($content[0], $content[3], $content[2], $content[1], $file_name);

        $body = new \SendGrid\Content("text/html", $message);
        $mail = new \SendGrid\Mail(self::$from, $subject, $to, $body);

        $att1 = new \SendGrid\Attachment();
        $att1->setContent(base64_encode(file_get_contents(__DIR__."/views/pdfs/".$file_name)));
        $att1->setType("application/pdf");
        $att1->setFilename($file_name);
        $mail->addAttachment($att1);

        $response = self::$sendgrid->client->mail()->send()->post($mail);

        return $response;
    }

}