import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';

import 'quasar-framework/dist/quasar.mat.css';
import 'quasar-extras/roboto-font';
import 'quasar-extras/material-icons';
import 'quasar-extras/fontawesome';
import 'quasar-extras/animate/bounceInRight.css'
import 'quasar-extras/animate/bounceOutRight.css'
import Quasar,  {Ripple,BackToTop, Platform} from 'quasar-framework';
Platform.has.popstate = false;

import Moment from 'vue-moment'

window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(Quasar, {
    directives: {
        Ripple,
        BackToTop
    }
});
Vue.use(Moment)

window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error(
        'CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token'
    );
}


