import VueRouter from 'vue-router'

let routes = [
    {
        path: '/',
        redirect: '/borrowers'
    },
    {
        path: '/borrowers',
        component: require('./views/Borrowers.vue')
    },
    {
        path: '/lenders',
        component: require('./views/Lenders.vue')
    },
    {
        path: '/loans',
        component: require('./views/Loans.vue')
    },
    {
        path: '/transactions',
        component: require('./views/Transactions.vue')
    },
    {
        path: '/schedule',
        component: require('./views/Schedule.vue')
    },
    {
        path: '/tokens',
        component: require('./views/Tokens.vue')
    },
    {
        path: '/logs',
        component: require('./views/Logs.vue')
    },
    {
        path: '/performance',
        component: require('./views/Performance.vue')
    }
]
export default new VueRouter({
    routes
})