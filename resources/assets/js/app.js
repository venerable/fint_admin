import './bootstrap';
import router from './routes';
import App from './views/App.vue';

import NProgress from 'vue-nprogress'
const nProgressOptions = {
    latencyThreshold: 200, // Number of ms before progressbar starts showing, default: 100,
    router: true, // Show progressbar when navigating routes, default: true
    http: false // Show progressbar when doing Vue.http, default: true
};
Vue.use(NProgress, nProgressOptions)

const nprogress = new NProgress({ parent: '.nprogress-container' })

// import Vuex from 'vuex'
// import appstore from './appstore'
// Vue.use(Vuex)
// const store = new Vuex.Store(appstore)

new Vue({
    el: '#app',
    // store,
    render: h => h(App),
    router,
    nprogress
});
