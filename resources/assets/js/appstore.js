export default {
    state: {
        borrowerID: '',
        loanID: ''
    }
    ,
    getters: {
        getBorrowerID: state => {
            return state.borrowerID
        },
        getLoanID: state => {
            return state.loanID
        }
    }
    ,
    mutations: {
        setBorrowerID(state, data) {
            state.borrowerID = data.borrower_id
        },
        setLoanID(state, data) {
            state.loanID = data.loan_id
        }
    }

}