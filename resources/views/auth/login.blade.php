<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin Login</title>

    <!-- Styles -->
    <link rel="shortcut icon" href="{{asset('favicon-32x32.png') }}"/>
    <link rel="icon" type="image/png" href="{{asset('favicon-32x32.png') }}">
    <link rel="apple-touch-icon" href="{{asset('favicon-32x32.png') }}">

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style type="text/css">
        @font-face {
            font-family: 'Avenir';
            src: url('{{asset('/fonts/AvenirLTStd-Book.woff2')}}') format('woff2'), /* Super Modern Browsers */
                url('{{asset('/fonts/AvenirLTStd-Book.woff')}}') format('woff'), /* Pretty Modern Browsers */
                url('{{asset('/fonts/AvenirLTStd-Book.ttf')}}') format('truetype'); /* Safari, Android, iOS */
        }

        body {
            font-family: "Avenir", sans-serif;
            background-color: #303641;
            font-weight: 200;
            font-size: 14px;
        }

        .panel {
            margin-top: 8%;
            padding: 5%;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 2px;
            max-width: 450px;
        }

        .form-control:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        input.form-control {
            height: 50px;
            font-size: 15px;
            border-radius: 2px;
        }

        button.btn-success {
            height: 50px;
            border: none;
            border-radius: 2px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div>
                <img class="center-block" src="{{asset('/images/fint_logo.png')}}"
                     alt="logo" width="40" height="40" style="margin-top: 4%"/>
            </div>
            <div class="panel panel-default center-block">
                {{--<div class="panel-heading">Login</div>--}}

                <div class="panel-body">
                    <span class="center-block text-center"
                          style="margin-top: 7px; margin-bottom: 25px; letter-spacing: 1px; font-size: 13px">LOG IN</span>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            {{--<label for="username" class="col-md-4 control-label">Username</label>--}}

                            <div class="col-md-12">
                                <input id="username" type="text" class="form-control" name="username"
                                       placeholder="Username"
                                       value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}"
                             style="margin-bottom: 20px">
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" placeholder="Password"
                                       name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success btn-block">
                                    <strong>Login</strong>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>
