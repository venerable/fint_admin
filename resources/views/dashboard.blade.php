<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Dashboard</title>

    <link rel="shortcut icon" href="{{asset('favicon.ico') }}"/>
    <link rel="icon" type="image/png" href="{{asset('favicon-32x32.png') }}">
    <link rel="apple-touch-icon" href="{{asset('favicon-32x32.png') }}">

    <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet" type="text/css"/>

</head>
<body>
<div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <div id="app"></div>
</div>

<script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>

</body>
</html>
